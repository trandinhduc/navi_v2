//
//  NVPaymentViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/16/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVPaymentViewController.h"
#import "NSString+Hashes.h"

#define ALERT_TAG_ERROR_CREATE_123PAY_PAYMENT 111
@interface NVPaymentViewController ()<UIWebViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)clickBackButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic) NSTimer *checkSuccess;
@property (nonatomic) NSInteger transactionStatus;
@property (nonatomic) NSString *transactionTicket;
@property (nonatomic) NSInteger countQueryOrder;

@end

@implementation NVPaymentViewController{

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.mediaPlaybackRequiresUserAction = true;
    self.webView.mediaPlaybackAllowsAirPlay = YES;
    self.webView.delegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlPayment]];
    [self.webView loadRequest:request];
    self.webView.hidden = YES;
}

-(void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	_countQueryOrder = 0;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [_indicator stopAnimating];
    NSString *link =[webView.request.mainDocumentURL absoluteString];
	
    self.webView.hidden = NO;
	
	if ([link rangeOfString:@"banksim/index.SMARTLINK"].location != NSNotFound) {// selecting Bank
		NSArray *components = [link componentsSeparatedByString:@"."];
		NSString *bankStr = [components objectAtIndex:components.count - 2];
		if (bankStr.length > 0 && [bankStr rangeOfString:@"123"].location != NSNotFound) {
			self.bankCode = bankStr;
		}
	}
    if ([link rangeOfString:@"navi.com.vn"].location != NSNotFound) {
		NSArray *comp1 = [link componentsSeparatedByString:@"?"];
		NSString *query = [comp1 lastObject];
		NSArray *queryElements = [query componentsSeparatedByString:@"&"];
		for (NSString *element in queryElements) {
			NSArray *keyVal = [element componentsSeparatedByString:@"="];
			if (keyVal.count > 0) {
				NSString *variableKey = [keyVal objectAtIndex:0];
				if ([variableKey isEqualToString:@"status"]) {
					NSString *value = (keyVal.count == 2) ? [keyVal lastObject] : @"0";
					self.transactionStatus = [value integerValue];
				}
				
				if ([variableKey isEqualToString:@"ticket"]) {
					self.transactionTicket	= (keyVal.count == 2) ? [keyVal lastObject] : @"0";
				}
			}
		}

		if (self.transactionStatus == 1) {
			[[Utils getInstance]shouldShowProgressHUD:YES WithText:@"Chờ thanh toán ..."];

			_checkSuccess = [NSTimer scheduledTimerWithTimeInterval:3.0
															 target:self
														   selector:@selector(checkPaymentStatus)
														   userInfo:nil
															repeats:YES];

//			NSDictionary *params = @{
//									 @"booking_id":self.bookingId,
//									 @"transaction_id":self.transactionId,
//									 @"transaction_status":@(self.transactionStatus),
//									 @"bank_code":self.bankCode,
//									 @"amount":@(self.amount),
//									 @"checksum":self.checkSum
//									 };
//			
//			[[NetworkingManager sharedInstance] createPaymentWithParams:params complete:^(BOOL success, id responseObject) {
//				if (success) {
//					_checkSuccess = [NSTimer scheduledTimerWithTimeInterval:3.0
//																	 target:self
//																   selector:@selector(checkPaymentStatus)
//																   userInfo:nil
//																	repeats:YES];
//				}else{
//					UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Có lỗi xảy ra khi thanh toán. Vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//					alert.tag = ALERT_TAG_ERROR_CREATE_123PAY_PAYMENT;
//					[alert show];
//				}
//			}];
		}else{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Có lỗi xảy ra khi thanh toán. Vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			alert.tag = ALERT_TAG_ERROR_CREATE_123PAY_PAYMENT;
			[alert show];
		}
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    return YES;
}

-(void)checkPaymentStatus{

	NSMutableString *checksum = [[NSMutableString alloc] initWithString:self.transactionId];
	[checksum appendString:merchantCode];
	[checksum appendString:clientIp];
	[checksum appendString:passcode];
	[checksum appendString:secrectCode];
	
    NSDictionary *params = @{@"mTransactionID":self.transactionId,
                             @"merchantCode":merchantCode,
                             @"clientIP":clientIp,
                             @"passcode":passcode,
                             @"checksum":[checksum sha1]
                             };
    [[NetworkingManager sharedInstance] wait123PayConfirm:params complete:^(BOOL success, id responseObject) {
        if (success) {
			 [self paymentFinishWithResponse:responseObject];
		}else{
			NSLog(@"query order ...%ld",(long)_countQueryOrder);
			_countQueryOrder ++;
			if (_countQueryOrder == 5) {
				[_checkSuccess invalidate];
				_checkSuccess = nil;
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Có lỗi xảy ra khi thanh toán. Vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
				alert.tag = ALERT_TAG_ERROR_CREATE_123PAY_PAYMENT;
				[alert show];
			}
		}
    }];
 
	
//	[[NetworkingManager sharedInstance] checkOrderSuccessWithTransactionId:_transactionId complete:^(BOOL success, id responseObject) {
//		if (success) {
//			[_checkSuccess invalidate];
//			_checkSuccess = nil;
//			[self.navigationController popViewControllerAnimated:NO];
//			[self.parentBooking finishPayment];
//		}else{
//			
//		}
//	}];
}

-(void)paymentFinishWithResponse:(id)responseObject{
	[[Utils getInstance] shouldShowProgressHUD:NO WithText:@""];
    // -20 , 10 cancel
    if ([responseObject[2] integerValue] == 1) {
        NSLog(@"Thanh toan thanh cong");
        [_checkSuccess invalidate];
        _checkSuccess = nil;
        [self.navigationController popViewControllerAnimated:NO];
        [self.parentBooking finishPayment];
    }else if([responseObject[2] integerValue] == -100 || [responseObject[2] integerValue] == -20){
        [_checkSuccess invalidate];
        _checkSuccess = nil;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Có lỗi xảy ra khi thanh toán. Vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		alert.tag = ALERT_TAG_ERROR_CREATE_123PAY_PAYMENT;
		[alert show];
    }else{
        NSLog(@"%@",responseObject[2]);
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lỗi" message:@"Có lỗi xảy ra khi thanh toán. Vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
		alert.tag = ALERT_TAG_ERROR_CREATE_123PAY_PAYMENT;
		[alert show];
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [_indicator startAnimating];
    NSString *link = [webView.request.mainDocumentURL absoluteString];
    NSLog(@"start load, %@" , link);
}

- (IBAction)clickBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (alertView.tag == ALERT_TAG_ERROR_CREATE_123PAY_PAYMENT) {
		[[Utils getInstance] shouldShowProgressHUD:NO WithText:@""];
		[self.navigationController popViewControllerAnimated:NO];
	}
}

@end
