//
//  NVPaymentViewController.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/16/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NVEnterBookingViewController.h"
@interface NVPaymentViewController : UIViewController
@property (nonatomic) NSString *urlPayment;

@property (nonatomic) NSString *transactionId;
@property (nonatomic) NSString *bookingId;
@property (nonatomic) NSString *checkSum;
@property (nonatomic) NSString *bankCode;
@property (nonatomic) NSInteger amount;

@property (nonatomic) NVEnterBookingViewController *parentBooking;
@end
