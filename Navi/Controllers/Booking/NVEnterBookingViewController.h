//
//  NVEnterBookingViewController.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/15/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Hotel.h"
#import "RoomDetail.h"
@interface NVEnterBookingViewController : UIViewController
@property (nonatomic) Hotel *hotelItem;
@property (nonatomic) RoomDetail *roomDetail;
-(void)finishPayment;
@end
