//
//  NVEnterBookingViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/15/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

//TODO: check login book before the current time

#import "NVEnterBookingViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSString+Hashes.h"
#import "NVPaymentViewController.h"
#import "NVConfirmPhoneView.h"
#import "NVFinishPaymentView.h"
#import "HotelDetail.h"
typedef enum bookingTypes
{
    BOOKING_DAY,
    CHECKIN_TIME,
    CHECKOUT_TIME
} bookingType;

@interface NVEnterBookingViewController () <UIPickerViewDelegate,UIGestureRecognizerDelegate,NVConfirmPhoneDelegate,NVFinishPaymentDelegate>

- (IBAction)clickOutsideTimePicker:(id)sender;
- (IBAction)clickBackButton:(id)sender;
- (IBAction)clickContinuesButton:(UIButton *)sender;

@property (nonatomic) NSDate *selectedDate;
@property (nonatomic) NSDate *selectedCheckInTime;
@property (nonatomic) NSDate *selectedCheckOutTime;

@property (nonatomic) NVConfirmPhoneView* confirmPhoneView;
@property (nonatomic) NVFinishPaymentView *finishView;
@property (nonatomic) bookingType datePickerType;
@property (nonatomic) NSString *bookingId;

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UILabel *bookingDayLabel;
@property (strong, nonatomic) IBOutlet UILabel *checkInLabel;
@property (strong, nonatomic) IBOutlet UILabel *checkOutLabel;
@property (strong, nonatomic) IBOutlet UILabel *hotelAddress;
@property (strong, nonatomic) IBOutlet UILabel *hotelName;
@property (strong, nonatomic) IBOutlet UILabel *hotelStar;
@property (strong, nonatomic) IBOutlet UILabel *statusBarTittle;
@property (strong, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (strong, nonatomic) IBOutlet UIView *bookingDayView;
@property (strong, nonatomic) IBOutlet UIView *checkInTimeView;
@property (strong, nonatomic) IBOutlet UIView *checkOutTimeView;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIView *totalView;
@property (weak, nonatomic) IBOutlet UIButton *tittleButton;
@property (weak, nonatomic) IBOutlet UIImageView *roomImageView;
@property (weak, nonatomic) IBOutlet UILabel *checkInDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkOutDateLabel;

@end

@implementation NVEnterBookingViewController{
    NSInteger bookingDay;
    NSInteger checkInTime;
    NSInteger checkOutTime;
    float totalHour;
    NSInteger totalPrice;
    NSString *checkInBookingHour;
    NSString *checkInBookingMiniute;
    NSString *checkOutBookingMiniute;
    NSString *currentHour;
    NSString *currentMinute;
    NSString *mTransactionID;
    NSString *checkOutBookingHour;
    NSArray *listMinute;
    BOOL isCheckIn;
    BOOL isNoneDuration;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isNoneDuration = NO;
    bookingDay = [[NSDate date] timeIntervalSince1970];
    listMinute = [NSArray arrayWithObjects:@"15",@"30",@"45",@"60",nil];
    
    [self updateCheckInTime:[NSDate dateWithTimeIntervalSince1970:[self getCurrentTime]]];
    [self generalLayoutHeader];
    
    UITapGestureRecognizer *clickCheckOutHour = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickChooseCheckOutTime)];
    clickCheckOutHour.numberOfTapsRequired = 1;
    clickCheckOutHour.delegate = self;
    [self.checkOutTimeView addGestureRecognizer:clickCheckOutHour];
    
    UITapGestureRecognizer *clickCheckInHour = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickChooseCheckInTime)];
    clickCheckInHour.numberOfTapsRequired = 1;
    clickCheckInHour.delegate = self;
    [self.checkInTimeView addGestureRecognizer:clickCheckInHour];
	
}

- (void)generalLayoutHeader{
    _hotelName.text = _hotelItem.name.uppercaseString;
    _hotelAddress.text = _hotelItem.address;
    NSString *starString = @"";
    switch (_hotelItem.star) {
        case 1:
            starString = [NSString stringWithUTF8String:"\u2605 "];
            break;
        case 2:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 "];
            break;
        case 3:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 "];
            break;
        case 4:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 \u2605 "];
            break;
        case 5:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 \u2605 \u2605 "];
            break;
        default:
            break;
    }
    _hotelStar.text = starString;
	if (_roomDetail.images.count > 0) {
		[Utils imageView:_roomImageView setImageWithURL:[Utils imageRoomUrl:_roomDetail.images[0]] placeholder:[UIImage imageNamed:@"hotel_default"]];
	}
}

#pragma mark - Caculate price
//
- (void)caculateTotalPrice{
    if (checkInTime < checkOutTime) {
		NSTimeInterval time = checkOutTime - checkInTime;
        totalHour = [self stringFromTimeInterval:time];
//        totalPrice = [self totalPrice:totalHour listPrice:_roomDetail.prices];
    }else{
        if(checkInTime == checkOutTime){
            totalHour = 0;
        }else{
            NSCalendar *c = [NSCalendar currentCalendar];
            NSDateComponents *components = [c components:NSCalendarUnitHour fromDate:[NSDate dateWithTimeIntervalSince1970:checkInTime] toDate:[NSDate dateWithTimeIntervalSince1970:checkOutTime] options:0];
            NSInteger diff = components.hour;
            if (diff < 0) {
                totalHour = 24 + diff;
            }
        }
        totalPrice = [self totalPrice:totalHour listPrice:_roomDetail.prices];
    }
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",(long)totalPrice]]];
}

- (NSInteger)totalPrice:(NSInteger)hours listPrice:(NSArray *)listPrice{
    NSInteger total = 0;
	if (listPrice.count > 0) {
		for (int i = 0; i<hours; i++) {
			total += [listPrice[(int)fminf(i, listPrice.count-1)] floatValue];
		}		
	}
    return total;
}

- (float )stringFromTimeInterval:(NSTimeInterval)interval {
//    float total;
    NSInteger ti = (NSInteger)interval;
//    NSInteger minutes = (ti / 60) % 60;
    float hours = (ti / 3600.0f);
//    if (minutes >0) {
//        total =hours + minutes/60.0f;
//        return total;
//    }
    return hours;
}

#pragma mark - Click choose booking time

- (void)clickChooseBookingDay{
    _datePickerType = BOOKING_DAY;
    self.selectedDate = [NSDate dateWithTimeIntervalSince1970:bookingDay];
    [self datePickerDidTapped:nil];
}

- (void)clickChooseCheckInTime{
    _datePickerType = CHECKIN_TIME;
     self.selectedCheckInTime = [NSDate dateWithTimeIntervalSince1970:checkInTime];
    [self datePickerDidTapped:nil];
}

- (void)clickChooseCheckOutTime{
    _datePickerType = CHECKOUT_TIME;
    self.selectedCheckOutTime = [NSDate dateWithTimeIntervalSince1970:checkOutTime];
    [self datePickerDidTapped:nil];
}

- (IBAction)datePickerDidTapped:(id)sender{
    [self.view endEditing:YES];
    self.pickerView.hidden = NO;
    self.datePicker.minimumDate =nil;
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    [self.datePicker setLocale:locale];
    
//    self.datePicker.maximumDate = nil;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    [comps setDay:+7];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    self.datePicker.minimumDate = minDate;
    self.datePicker.maximumDate = maxDate;

    if (_datePickerType == BOOKING_DAY) {
        [self.datePicker setDatePickerMode:UIDatePickerModeDate];
        
//        NSDate *currentDate = [NSDate date];
//        NSDateComponents *comps = [[NSDateComponents alloc] init];
//        NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
//        [comps setDay:+7];
//        NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
//        self.datePicker.minimumDate = minDate;
//        self.datePicker.maximumDate = maxDate;
    }else{
        if (_datePickerType == CHECKIN_TIME) {
//            NSTimeInterval checkOutTimeStamp = 60 * 60;
//            NSDate *chooseDateCheckOutTime = [_selectedCheckInTime dateByAddingTimeInterval:checkOutTimeStamp];
//            self.datePicker.minimumDate = chooseDateCheckOutTime;

            if ([self isEndDateIsSmallerThanCurrent:self.selectedDate]) {
                self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSince1970:[self getCurrentTime]];
            }else {
                NSDate *choosenDate = [gregorian dateBySettingHour:0 minute:0 second:0 ofDate:_selectedDate options:0];
                self.datePicker.minimumDate = choosenDate;
            }
            [self.datePicker setDate:[NSDate dateWithTimeIntervalSince1970:checkInTime]];
        }else if (_datePickerType == CHECKOUT_TIME){
            NSDate *currentDate = [NSDate date];
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:-1];
            NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
            self.datePicker.minimumDate =[NSDate dateWithTimeIntervalSince1970:checkInTime];
            [self.datePicker setDate:[NSDate dateWithTimeIntervalSince1970:checkOutTime]];
        }
        self.datePicker.minuteInterval = 15;
//        [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    }
    [UIView animateWithDuration:0.5f animations:^{
        self.pickerView.alpha = 1;
    } completion:^(BOOL finished) {
        if (self.selectedDate) {
            if (_datePickerType == CHECKIN_TIME) {
                NSDate *checkInTimee;
                if ([self isEndDateIsSmallerThanCurrent:self.selectedDate]) {
                    checkInTimee =[NSDate dateWithTimeIntervalSince1970:[self getCurrentTime]];
                }else {
                    NSDate *choosenDate = [gregorian dateBySettingHour:0 minute:0 second:0 ofDate:_selectedDate options:0];
                    checkInTimee =choosenDate;
                }
                [self.datePicker setDate:checkInTimee];
            }else if (_datePickerType == CHECKOUT_TIME){
//                NSTimeInterval checkOutTimeStamp = 60 * 60;
//                NSDate *chooseDateCheckOutTime = [_selectedCheckInTime dateByAddingTimeInterval:checkOutTimeStamp];
//                [self.datePicker setDate:chooseDateCheckOutTime];
            }else{
                [self.datePicker setDate:self.selectedDate];
                [self updateCheckInTime:self.selectedDate];
            }
        }
    }];
}

- (IBAction)okPickDateDidTapped:(id)sender{
    if (_datePickerType == BOOKING_DAY) {
        _selectedDate = self.datePicker.date;
        bookingDay = [_selectedDate timeIntervalSince1970];
        [self updateTimeForBookingDay:_selectedDate];
    }else if (_datePickerType == CHECKIN_TIME){
        _selectedCheckInTime = self.datePicker.date;
        [self updateCheckInTime:_selectedCheckInTime];
    }else{
        _selectedCheckOutTime = self.datePicker.date;
        [self updateCheckOutTime:_selectedCheckOutTime];
    }
    
    [self cancelPickDateDidTapped:nil];
//    [self caculateTotalPrice];
}

#pragma mark - Update booking time
- (void)updateTimeForBookingDay:(NSDate *)date{
    if ([self checkToday]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        NSString *timePicker =[formatter stringFromDate:date];
        if ([timePicker isEqualToString:[formatter stringFromDate:[NSDate date]]]) {
            self.bookingDayLabel.text = @"HÔM NAY";
        }else{
            self.bookingDayLabel.text = timePicker;
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Ngày bạn chọn nhỏ hơn ngày hiện tại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    [self updateCheckInTime:date];
}

- (void)updateCheckInTime:(NSDate *)date{
    checkInTime = [date timeIntervalSince1970];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    NSString *timePicker =[formatter stringFromDate:date];
    self.checkInLabel.text = timePicker;
    
    [formatter setDateFormat:@"d/MM/YY"];
    NSString *dateString = [formatter stringFromDate:date];
    self.checkInDateLabel.text = dateString;
    
    NSLog(@"Current Date: %@", [formatter stringFromDate:date]);
   
    NSTimeInterval secondsInEightHours = 60 * 60;
    [self updateCheckOutTime:[date dateByAddingTimeInterval:secondsInEightHours]];
}

- (void)updateCheckOutTime:(NSDate *)date{
    checkOutTime = [date timeIntervalSince1970];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSString *timePicker =[formatter stringFromDate:date];
    self.checkOutLabel.text = timePicker;
    
    [formatter setDateFormat:@"d/MM/YY"];
    NSString *dateString = [formatter stringFromDate:date];
    self.checkOutDateLabel.text = dateString;
    
    [self caculatePrice:[NSDate dateWithTimeIntervalSince1970:checkInTime] and:date];
}

- (BOOL)checkToday{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsForFirstDate = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:[NSDate dateWithTimeIntervalSince1970:bookingDay]];
    
    NSDateComponents *componentsForSecondDate = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:[NSDate date]];
    
    if ([componentsForFirstDate day] >= [componentsForSecondDate day]){
        return YES;
    }
    return NO;
}

#pragma mark - Picker booking time tapped
- (IBAction)cancelPickDateDidTapped:(id)sender{
    [UIView animateWithDuration:0.5f animations:^{
        self.pickerView.alpha = 0.f;
    } completion:^(BOOL finished) {
        self.pickerView.hidden = YES;
    }];
}

- (IBAction)clickOutsideTimePicker:(id)sender {
    [self cancelPickDateDidTapped:nil];
}

- (IBAction)clickBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickContinuesButton:(UIButton *)sender {
    if ([self check60Minute]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Vui lòng chọn tối thiểu 60 phút để đặt phòng." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    NSTimeInterval time = checkOutTime - checkInTime;
    totalHour = [self stringFromTimeInterval:time];
    
    if (totalHour > 24) {
        UIAlertView *alert24Hour = [[UIAlertView alloc] initWithTitle:@"" message:@"Vui lòng chọn tối đa 24 giờ để đặt phòng." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert24Hour show];
        return;
    }
    
    if (isNoneDuration) {
        isNoneDuration = NO;
        UIAlertView *alertNonDuration = [[UIAlertView alloc] initWithTitle:@"" message:CustomLocalisedString(@"isNoneDuration", @"isNoneDuration") delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertNonDuration show];
        return;
    }
    
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"HH:mm"];
	NSString *checkIn =[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkInTime]];
	NSString *checkout = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkOutTime]];

    [[NetworkingManager sharedInstance] checkAvailableRoomsInHotel:_hotelItem.hotelId andRoom:_roomDetail.roomId checkIn:checkIn checkOut:checkout time: totalHour executedTime:checkInTime complete:^(BOOL success, id responseObject) {
        if (success) {
            //    NVConfirmPhoneView *confirmPhoneView = [[NVConfirmPhoneView alloc]initWithFrame:self.view.frame];
            _confirmPhoneView = [[[NSBundle mainBundle] loadNibNamed:[NVCacheManager sharedInstance].ConfirmInputPhoneName owner:self options:nil] objectAtIndex:0];
            CGRect frame = self.view.bounds;
            _confirmPhoneView.frame = frame;
            _confirmPhoneView.delegate = self;
            [self.view addSubview:_confirmPhoneView];
        }else{
            [Utils showAlertWithTitle:@"Thông báo" mess:@"Khung giờ này hiện hết phòng, vui lòng chọn lại!"];
        }
    }];
}
// Check duration avalaible
-(BOOL)checkDurationWithCheckInTime:(NSInteger )checkInTime CheckOutTime:(NSInteger )checkOutTime{
//    NSString *dayCheckIn;
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"EEE"];
//    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
//    [formatter setLocale:usLocale];
//    dayCheckIn = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkInTime]];
//    NSString *dayCheckOut;
//    dayCheckOut = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkOutTime]];
//    NSArray *listRangeInWeek = [_roomDetail.listPrice allKeys];
//    NSLog(@"%@",listRangeInWeek);
////
//    if ([dayCheckIn isEqualToString:dayCheckOut]) {
//        price = 0;
//        NSDictionary *listPriceInDay;// = [_roomDetail.listPrice valueForKey:dayCheckIn];
//        if ([@"Tue-Wed-Thu" rangeOfString:dayCheckIn].location == NSNotFound) {
//            listPriceInDay = [_roomDetail.listPrice valueForKey:dayCheckIn];
//        }else{
//            listPriceInDay = [_roomDetail.listPrice valueForKey:@"Tue-Wed-Thu"];
//        }
    return YES;
}

-(BOOL)check60Minute{
    NSInteger totalTime = checkOutTime - checkInTime;
    NSDateComponentsFormatter *dateComponentsFormatter = [[NSDateComponentsFormatter alloc] init];
    dateComponentsFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    dateComponentsFormatter.allowedUnits = (NSCalendarUnitHour);
    NSString *hourAndMinute =  [dateComponentsFormatter stringFromTimeInterval:totalTime];
    if ([hourAndMinute integerValue]< 1) {
        return YES;
    }
    return NO;
}

- (void)bookingWithPhoneNumber:(NSString *)phoneNumber{
    [self clickOutSideConfirmPhone];
	[self makeBookingWithPhoneNumber:phoneNumber complete:^(BOOL success) {
		if (success) {
			[self sendPaymentWithPhoneNumber:phoneNumber];
		}
		else{
			UIAlertView *alertErrorNetwork = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Có lỗi xảy ra. Xin vui lòng kiểm tra lại các thông tin." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
			[alertErrorNetwork show];
		}
	}];
}

- (void)clickOutSideConfirmPhone{
    [_confirmPhoneView removeFromSuperview];
}

- (void)sendPaymentWithPhoneNumber:(NSString *)phoneNumber{
	[_confirmPhoneView removeFromSuperview];
	/*mTransactionID+merchantCode+bankCode+ totalAmount+ clientIP+custName + custAddress+custGender + custDOB + custPhone+custMail + cancelURL+redirectURL+errorURL+ passcode + secretKey(123Pay cung cấp)*/
	NSMutableString *checksum = [[NSMutableString alloc] initWithString:mTransactionID];
	[checksum appendString:merchantCode];
	[checksum appendString:BANK_CODE];
	[checksum appendFormat:@"%ld",totalPrice];
	[checksum appendString:@"127.0.0.1"];// client id
	[checksum appendString:@"Unknow"];// customer Name
	[checksum appendString:@"Unknow"];// customer Address
	[checksum appendString:@"U"];// customer Gender
	[checksum appendString:@"01/01/1970"];// customer birthday
	[checksum appendString:phoneNumber];// customer phone
	[checksum appendString:@"an.nn@geekup.vn"];// customer email
	[checksum appendString:CANCEL_URL];
	[checksum appendString:REDIRECT_URL];
	[checksum appendString:ERROR_URL];
	[checksum appendString:passcode];
	[checksum appendString:secrectCode];
	
    NSDictionary *params = @{@"mTransactionID":mTransactionID,
                             @"merchantCode":merchantCode,
                             @"bankCode":BANK_CODE,
                             @"totalAmount":@(totalPrice),
                             @"clientIP":@"127.0.0.1",
                             @"custDOB":@"01/01/1970",
                             @"custName":@"Unknow",
                             @"custAddress":@"Unknow",
                             @"custGender":@"U",
                             @"custPhone":phoneNumber,
                             @"custMail":@"an.nn@geekup.vn",
                             @"description":@"Dat phong tai NAVI Booking",
                             @"cancelURL":CANCEL_URL,
							 @"redirectURL":REDIRECT_URL,
                             @"errorURL":ERROR_URL,
							 @"checksum":[checksum sha1],
                             @"passcode":passcode
                             };
    [[Utils getInstance] shouldShowProgressHUD:YES WithText:@"Vui lòng đợi..."];
    [[NetworkingManager sharedInstance] sendOrderTo123Pay:params complete:^(BOOL success, id responseObject) {
        [[Utils getInstance] shouldShowProgressHUD:NO WithText:@""];
        if (success) {
            [self goToPaymentScreenWithResponse:responseObject];
        }else{
            if ([responseObject isKindOfClass:[NSError class]]) {
                UIAlertView *alertErrorNetwork = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Có lỗi xảy ra. Xin vui lòng kiểm tra lại các thông tin." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertErrorNetwork show];
            }else{
                UIAlertView *alertErrorPayment = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:responseObject delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertErrorPayment show];
            }
        }
    }];
}

- (void)goToPaymentScreenWithResponse:(id)responseObject{

    NVPaymentViewController *paymentVC = [[NVPaymentViewController alloc]init];
	if ([responseObject count] > 3) {
		paymentVC.urlPayment = responseObject[2];
		paymentVC.transactionId = mTransactionID;//responseObject[1];
		paymentVC.checkSum = responseObject[3];
	}
	paymentVC.bankCode = BANK_CODE;
	paymentVC.amount = totalPrice;
    paymentVC.bookingId = self.bookingId;
    paymentVC.parentBooking = self;
    [self.navigationController pushViewController:paymentVC animated:YES];
}

- (void)finishPayment{
	_finishView = [[[NSBundle mainBundle] loadNibNamed:[NVCacheManager sharedInstance].finishPaymentName owner:self options:nil] objectAtIndex:0];
	CGRect frame = self.view.bounds;
	_finishView.frame = frame;
	_finishView.delegate = self;
	[self.view addSubview:_finishView];
}

- (void)makeBookingWithPhoneNumber:(NSString *)phoneNumber complete:(void (^)(BOOL success))complete{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"HH:mm"];
	NSString *checkIn =[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkInTime]];
	NSString *checkout = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkOutTime]];
	mTransactionID = [NSString stringWithFormat:@"navi%0.f",[[NSDate date] timeIntervalSince1970]];
	[[NetworkingManager sharedInstance] confimBookingWithHotel:_hotelItem.hotelId andRoom:_roomDetail.roomId checkIn:checkIn checkOut:checkout time: totalHour executedTime:checkInTime phoneUser:phoneNumber  amount:totalPrice transactionId:mTransactionID complete:^(BOOL success, id responseObject) {
		if (success) {
			_bookingId = responseObject[@"booking_id"];
			complete (true);
		}else{
//			if ([responseObject isKindOfClass:[NSError class]]) {
//				UIAlertView *alertErrorNetwork = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Có lỗi xảy ra. Xin vui lòng kiểm tra lại các thông tin." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//				[alertErrorNetwork show];
//			}else{
////				UIAlertView *alertErrorPayment = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:responseObject delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
////				[alertErrorPayment show];
//			}
			complete (false);
		}
	}];

}

- (void)clickFinish{
    [_finishView removeFromSuperview];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UIPicker Datasoure Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate {
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}

-(void)caculatePrice:(NSDate *)startTime and:(NSDate *)endTime{
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"HH:mm"];
//    NSArray *startTimeHourList = [[formatter stringFromDate:startTime] componentsSeparatedByString:@":"];
//    NSArray *endTimeHourList = [[formatter stringFromDate:endTime] componentsSeparatedByString:@":"];
//    
//    [formatter setDateFormat:@"dd"];
//    NSString *todayDate = [formatter stringFromDate:[NSDate date]];
//    NSString *startDate = [formatter stringFromDate:startTime];
//    NSString *endDate = [formatter stringFromDate:endTime];
    
    // Trong ngày
//    NSString *day = [self getToDay];
//    if ([startDate isEqualToString:endDate] && [startDate isEqualToString:todayDate]) {
//        NSInteger totalHourPicked = [endTimeHourList[0] integerValue] - [startTimeHourList[0] integerValue];
//        NSInteger totalPriceTemp = 0;
//        for (int i = 0; i < totalHourPicked; i++) {
//            NSInteger itemHour = [startTimeHourList[0] integerValue] + i;
//            NSInteger itemPrice;
//            if ([@"Tue-Wed-Thu" rangeOfString:day].location == NSNotFound) {
//                itemPrice = [[[_roomDetail.listPrice valueForKey:day] valueForKey:[NSString stringWithFormat:@"%ld",(long)itemHour]] integerValue];
//            }else{
//                itemPrice = [[[_roomDetail.listPrice valueForKey:@"Tue-Wed-Thu"] valueForKey:[NSString stringWithFormat:@"%ld",(long)itemHour]] integerValue];
//            }
//
////            NSInteger itemPrice = [[_roomDetail.listPrice valueForKey:[NSString stringWithFormat:@"%ld",(long)itemHour]] integerValue];
//            totalPriceTemp = totalPriceTemp + itemPrice;
//        }
//        totalPrice = totalPriceTemp;
//        self.totalPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",(long)totalPrice]]];
//    }
    
    
    // Từ ngày này qua ngày khác
//    if (![startDate isEqualToString:todayDate]) {
//        [formatter setDateFormat:@"EEE"];
//        NSString *dayString = [formatter stringFromDate:startTime];
//        
//         [[Utils getInstance]shouldShowProgressHUD:YES WithText:@""];
//        [[NetworkingManager sharedInstance] loadHotelDetailWithHotelId:_hotelItem.hotelId withDate:dayString complete:^(BOOL success, id responseObject) {
//            if (success) {
//                NSLog(@"%@",responseObject);
//                HotelDetail *hotelDetailTempt = [HotelDetail new];
//                hotelDetailTempt = [HotelDetail convertJsonToHotelDetail:responseObject];
//                
//                for (RoomDetail *roomItem in hotelDetailTempt.listRoom) {
//                    if (roomItem.roomId == _roomDetail.roomId) {
//                        NSInteger totalHourPicked = [endTimeHourList[0] integerValue] - [startTimeHourList[0] integerValue];
//                        NSInteger totalPriceTemp = 0;
//                        for (int i = 0; i < totalHourPicked; i++) {
//                            NSInteger itemHour = [startTimeHourList[0] integerValue] + i;
//                            NSInteger itemPrice = [[roomItem.listPrice valueForKey:[NSString stringWithFormat:@"%ld",(long)itemHour]] integerValue];
//                            totalPriceTemp = totalPriceTemp + itemPrice;
//                        }
//                        totalPrice = totalPriceTemp;
//                        self.totalPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",(long)totalPrice]]];
//                    }
//                }
//                
//            }else{
//                
//            }
//            [[Utils getInstance]shouldShowProgressHUD:NO WithText:@""];
//        }];
//    }

    // Ngày khác
    
    [self caculateCheckInTime:checkInTime CheckOutTime:checkOutTime];
//    NSString *hourString = [formatter stringFromDate:startTime];
//    [formatter setDateFormat:@"d/MM/YY"];
//    NSString *dateString = [formatter stringFromDate:startTime];
//    NSLog(@"%@",_roomDetail.listPrice);
}

-(NSString *)getToDay{
    NSString *toDay;
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:usLocale];
    toDay = [formatter stringFromDate:date];
    return toDay;
}

-(NSInteger)getCurrentTime{
    NSInteger currentTime = [[NSDate date]timeIntervalSince1970];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"mm"];
    NSString *minute = [formatter stringFromDate:[NSDate date]];
    for (int i = 0; i < listMinute.count; i++) {
        if ([minute integerValue] < [listMinute[i] integerValue]) {
            currentTime = currentTime + (60*([listMinute[i] integerValue] - [minute integerValue]));
            return currentTime;
        }
    }
    return currentTime;
}

-(void)caculateCheckInTime:(NSInteger )checkInTimeInput CheckOutTime:(NSInteger)checkOutTimeInput{
    NSInteger hourCheckIn;
    NSInteger minuteCheckIn;
    NSInteger hourCheckOut;
    NSInteger minuteCheckOut;
    NSInteger priceIn15Minute;
    NSInteger price;
    
    NSDateFormatter *formatterHour = [[NSDateFormatter alloc] init];
    [formatterHour setDateFormat:@"HH:mm"];
    NSArray *startTimeHourList = [[formatterHour stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkInTimeInput]] componentsSeparatedByString:@":"];
    NSArray *endTimeHourList = [[formatterHour stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkOutTimeInput]] componentsSeparatedByString:@":"];
    
    hourCheckIn = [startTimeHourList[0]integerValue];
    minuteCheckIn = [startTimeHourList[1] integerValue];
    hourCheckOut = [endTimeHourList[0]integerValue];
    minuteCheckOut = [endTimeHourList[1]integerValue];
    
    NSString *dayCheckIn;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:usLocale];
    dayCheckIn = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkInTimeInput]];
    NSString *dayCheckOut;
    dayCheckOut = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:checkOutTimeInput]];
    NSArray *listRangeInWeek = [_roomDetail.listPrice allKeys];
    NSLog(@"%@",listRangeInWeek);
    
    if ([dayCheckIn isEqualToString:dayCheckOut]) {
        price = 0;
        NSDictionary *listPriceInDay;// = [_roomDetail.listPrice valueForKey:dayCheckIn];
         if ([@"Tue-Wed-Thu" rangeOfString:dayCheckIn].location == NSNotFound) {
            listPriceInDay = [_roomDetail.listPrice valueForKey:dayCheckIn];
         }else{
            listPriceInDay = [_roomDetail.listPrice valueForKey:@"Tue-Wed-Thu"];
         }
        if (minuteCheckIn > 0 || minuteCheckOut > 0) {
            for (int i=0; i <= hourCheckOut - hourCheckIn; i++) {
                priceIn15Minute = [[listPriceInDay valueForKey:[NSString stringWithFormat:@"%ld",hourCheckIn + i]]integerValue]/4;
                if (priceIn15Minute == 0) {
                    isNoneDuration = YES;
                }
                if (i == 0) {
                    price = price + (priceIn15Minute*((60-minuteCheckIn)/15));
                }else if (i == (hourCheckOut - hourCheckIn)) {
                    price = price + (priceIn15Minute*((minuteCheckOut)/15));
                }else{
                    price = price + priceIn15Minute*4;
                }
                priceIn15Minute = 0;
            }
            totalPrice = price;
            self.totalPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",(long)totalPrice]]];
        }else{
            NSInteger priceHour;
            for (int i =0; i < hourCheckOut - hourCheckIn; i ++) {
                priceHour = [[listPriceInDay valueForKey:[NSString stringWithFormat:@"%ld",hourCheckIn + i]]integerValue];
                if (priceHour == 0) {
                     isNoneDuration = YES;
                }
                price = price + priceHour;
                priceHour =0;
            }
            totalPrice = price;
            self.totalPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",(long)totalPrice]]];
        }
    }else{
        NSArray *listRange = [_roomDetail.listPrice allKeys];
        NSInteger totalTime = checkOutTimeInput - checkInTimeInput;
        NSDateComponentsFormatter *dateComponentsFormatter = [[NSDateComponentsFormatter alloc] init];
        dateComponentsFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
        dateComponentsFormatter.allowedUnits = (NSCalendarUnitHour | NSCalendarUnitMinute);
        NSString *hourAndMinute =  [dateComponentsFormatter stringFromTimeInterval:totalTime];
    
        dateComponentsFormatter.allowedUnits = NSCalendarUnitDay;
    
        NSString *dayString = [dateComponentsFormatter stringFromTimeInterval:totalTime];
        NSInteger totalDay = [[dayString stringByReplacingOccurrencesOfString:@"d" withString:@""] integerValue];
        NSInteger totalHourInput = [[hourAndMinute componentsSeparatedByString:@":"][0] integerValue];
        NSInteger minute = [[hourAndMinute componentsSeparatedByString:@":"][1] integerValue];
       
        if (totalHourInput < 24) {
            price = 0;
            NSDictionary *listPriceCheckInDay;// = [_roomDetail.listPrice valueForKey:dayCheckIn];
            if ([@"Tue-Wed-Thu" rangeOfString:dayCheckIn].location == NSNotFound) {
                listPriceCheckInDay = [_roomDetail.listPrice valueForKey:dayCheckIn];
            }else{
                listPriceCheckInDay = [_roomDetail.listPrice valueForKey:@"Tue-Wed-Thu"];
            }
             NSDictionary *listPriceCheckOutDay;
            if ([@"Tue-Wed-Thu" rangeOfString:dayCheckOut].location == NSNotFound) {
                listPriceCheckOutDay = [_roomDetail.listPrice valueForKey:dayCheckOut];
            }else{
                listPriceCheckOutDay = [_roomDetail.listPrice valueForKey:@"Tue-Wed-Thu"];
            }

            if (minuteCheckIn > 0 || minuteCheckOut > 0) {
                if (minute >0) {
                    totalHourInput = totalHourInput + 1;
                }
                for (int i=0; i <= totalHourInput; i++) {
                    if ((hourCheckIn + i) < 24) {
                        priceIn15Minute = [[listPriceCheckInDay valueForKey:[NSString stringWithFormat:@"%ld",hourCheckIn + i]]integerValue]/4;
                        if (priceIn15Minute == 0) {
                            isNoneDuration = YES;
                        }
                        if (i == 0) {
                            price = price + (priceIn15Minute*((60-minuteCheckIn)/15));
                        }else if (i == totalHourInput) {
                            price = price + (priceIn15Minute*((minuteCheckOut)/15));
                        }else{
                            price = price + priceIn15Minute*4;
                        }
                        priceIn15Minute = 0;
                        
                    }else{
                        priceIn15Minute = [[listPriceCheckInDay valueForKey:[NSString stringWithFormat:@"%ld",hourCheckOut - (totalHourInput - i)]]integerValue]/4;
                        if (priceIn15Minute == 0) {
                            isNoneDuration = YES;
                        }
                        if (i == totalHourInput) {
                            price = price + (priceIn15Minute*((minuteCheckOut)/15));
                        }else{
                            price = price + priceIn15Minute*4;
                        }
                        priceIn15Minute = 0;
                    }
                }
            }else{
                 NSInteger priceHour;
                for (int i =0; i< totalHourInput; i++) {
                    if ((hourCheckIn + i) < 24) {
                        priceHour = [[listPriceCheckInDay valueForKey:[NSString stringWithFormat:@"%ld",hourCheckIn + i]] integerValue];
                        if (priceHour == 0) {
                            isNoneDuration = YES;
                        }
                        price = price + priceHour;
                        priceHour = 0;
                    }else{
                        priceHour = [[listPriceCheckOutDay valueForKey:[NSString stringWithFormat:@"%ld",hourCheckOut - (totalHourInput - i)]] integerValue];
                        if (priceHour == 0) {
                            isNoneDuration = YES;
                        }
                        price = price + priceHour;
                        priceHour = 0;
                    }
                }
            }
            totalPrice = price;
            self.totalPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",(long)totalPrice]]];
        }else{
            
        }
//        for (int i =0; i<totalDay; i++) {
//            
//            
//        }
        
    }
    
//    NSDictionary *listHour;
//    NSInteger totalTime = checkOutTimeInput - checkInTimeInput;
//
//    NSInteger minuteOnAHour;
//    
//    for (int i=0; i < hourCheckOut - hourCheckIn; i++) {
//        if (i == 0) {
//            [listHour setValue:[NSString stringWithFormat:@"%ld",(60 - minuteCheckIn)] forKey:[NSString stringWithFormat:@"%ld",hourCheckIn+i]];
//        }
//        NSInteger priceThisHour = priceIn15Minute*((60-minuteCheckIn)/15);
//    }
//
//    
//    // Get time from totalTime
//    NSDateComponentsFormatter *dateComponentsFormatter = [[NSDateComponentsFormatter alloc] init];
//    dateComponentsFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
//    dateComponentsFormatter.allowedUnits = (NSCalendarUnitHour | NSCalendarUnitMinute);
//    NSString *hourAndMinute =  [dateComponentsFormatter stringFromTimeInterval:totalTime];
//    
//    dateComponentsFormatter.allowedUnits = NSCalendarUnitDay;
//    
//    NSString *dayString = [dateComponentsFormatter stringFromTimeInterval:totalTime];
//    NSInteger totalDay = [[dayString stringByReplacingOccurrencesOfString:@"d" withString:@""] integerValue];
//    NSInteger totalHour = [[hourAndMinute componentsSeparatedByString:@":"][0] integerValue];
//    NSInteger minute = [[hourAndMinute componentsSeparatedByString:@":"][1] integerValue];
//    NSLog(@"%@ %@", hourAndMinute, dayString);
//
////    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
////    [formatter setDateFormat:@"d:HH:mm"];
////    NSLog(@"%@",[formatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:totalTime]]);
//    
//    for (int i=0; i <= totalDay; i++) {
//        for (int i =0; i < [listHour allKeys].count; i++) {
//            NSLog(@"%@",[listHour valueForKey:[NSString stringWithFormat:@"%d",i]]);
//        }
//    }
    
    
    
}

@end
