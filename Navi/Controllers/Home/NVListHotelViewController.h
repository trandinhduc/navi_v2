//
//  NVListHotelViewController.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NVListHotelViewController : UIViewController
@property (nonatomic) NSInteger districtId;
@property (nonatomic) NSString *districtName;
@property (weak, nonatomic) IBOutlet UILabel *lbEmptyHotel;
@end
