//
//  NVHomeViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/12/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVHomeViewController.h"
#import "NVListHotelViewController.h"
#import "SWRevealViewController.h"
#import "District.h"
#import "SupportViewController.h"
@import Firebase;
@interface NVHomeViewController () <UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,SWRevealViewControllerDelegate>

@property (nonatomic) NSArray *arrayListDistrict;

@property (strong, nonatomic) IBOutlet UIButton *bgButton;
@property (strong, nonatomic) IBOutlet UIButton *choiceDistricButton;
@property (strong, nonatomic) IBOutlet UILabel *wellcomeLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableListDistrict;
@property (strong, nonatomic) IBOutlet UIView *listDistrictView;
@property (strong, nonatomic) LocationService *locationService;

@end

@implementation NVHomeViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    [FIRAnalytics logEventWithName:kFIREventSelectContent parameters:@{
                                                                       kFIRParameterContentType:@"cont",
                                                                       kFIRParameterItemID:@"1"
                                                                       }];
	_locationService = [[LocationService alloc] init];
	[_locationService startLocationService];
	
    _choiceDistricButton.layer.cornerRadius = 3;
    _choiceDistricButton.clipsToBounds = YES;
    _listDistrictView.layer.cornerRadius = 3;
    _listDistrictView.clipsToBounds = YES;
    _wellcomeLabel.adjustsFontSizeToFitWidth = YES;
    
    _tableListDistrict.layer.cornerRadius = 3;
    _tableListDistrict.clipsToBounds = YES;
    
    SWRevealViewController *revealController = [self revealViewController];
    revealController.delegate = self;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    [_tableListDistrict reloadData];
    
    UITapGestureRecognizer *clickBgButton = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickBackFromMenu:)];
    clickBgButton.delegate = self;
    [self.view addGestureRecognizer:clickBgButton];
	
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
    // Enable swipe back
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return ![NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"];
}

#pragma mark - IBActions

- (IBAction)clickMenuButton:(id)sender {
    [[AppDelegate sharedDelegate].viewController revealToggle:nil];
}

- (IBAction)clickChoiceDistricButton:(id)sender {
    [self.view bringSubviewToFront:_listDistrictView];
    [self.view bringSubviewToFront:_bgButton];
    _tableListDistrict.hidden = !_tableListDistrict.isHidden;

}

#pragma mark - Tableview delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *NaviCellIdentifier = @"NaviListDistrictCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NaviCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NaviCellIdentifier];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    District *districtItem = [NVCacheManager sharedInstance].listDistrict[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = districtItem.nameDistrict;
    if (IS_IPHONE_5 || IS_IPHONE_6) {
        [cell.textLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:14]];
    }else{
        [cell.textLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:20]];
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [NVCacheManager sharedInstance].listDistrict.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (IS_IPHONE_5 || IS_IPHONE_6) {
//        return 32;
//    }else{
        return 44;
//    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self clickChoiceDistricButton:nil];
    
    NVListHotelViewController *listHotelVC = [[NVListHotelViewController alloc]initWithNibName:[NVCacheManager sharedInstance].ListHotelViewName bundle:nil];
    District *districtItem = [NVCacheManager sharedInstance].listDistrict[indexPath.row];
    listHotelVC.districtId = districtItem.idDistrict;
    listHotelVC.districtName = districtItem.nameDistrict;
    [self.navigationController pushViewController:listHotelVC animated:YES];
}

#pragma mark - Delegates

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position{
    if (position == FrontViewPositionLeft) {
        NSLog(@"close");
        _bgButton.hidden = YES;
        [self enableDictrictSelection:YES];
        self.view.alpha = 1.0f;
    }else{
        _bgButton.hidden = NO;
        [self enableDictrictSelection:NO];
        self.view.alpha = 0.5f;
        NSLog(@"open");
    }
}

#pragma mark - private methods

- (void)enableDictrictSelection:(BOOL)enable{
    self.choiceDistricButton.enabled = enable;
    self.choiceDistricButton.userInteractionEnabled = enable;
}

//// for ip6P, optimize later
//- (void)dismissAllMenu{
//    _listDistrictView.hidden = YES;
//}

#pragma mark - Selector

- (void)clickBackFromMenu:(UITapGestureRecognizer *)tapGesture{
    [self clickMenuButton:nil];
    _bgButton.hidden = YES;
}

@end
