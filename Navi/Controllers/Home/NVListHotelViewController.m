//
//  NVListHotelViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVListHotelViewController.h"
#import "NVHotelViewCell.h"
#import "NVDetailHotelViewController.h"
#import "Hotel.h"
#import "NVEnterBookingViewController.h"
#import "SWRevealViewController.h"
#import "District.h"

@interface NVListHotelViewController ()<UITableViewDelegate,UITableViewDataSource, SWRevealViewControllerDelegate, UIGestureRecognizerDelegate>

@property (nonatomic) NSInteger currentPage;
@property (nonatomic) NSInteger totalPage;
@property (nonatomic) NSMutableArray *listHotels;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIButton *bgButton;
@property (strong, nonatomic) IBOutlet UIButton *choiceDistricButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (strong, nonatomic) IBOutlet UIView *emptyLabel;
@property (strong, nonatomic) IBOutlet UILabel *tittleDistrict;
@property (strong, nonatomic) IBOutlet UITableView *tableListHotel;
@property (strong, nonatomic) IBOutlet UITableView *tableListDictrict;
@property (weak, nonatomic) IBOutlet UIButton *tittleButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic , strong) LocationService *locationService;
@end

@implementation NVListHotelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_listHotels == nil) {
        _listHotels = [NSMutableArray new];
    }
	self.tableListDictrict.layer.cornerRadius = 6.f;
	self.tableListDictrict.layer.borderWidth = 1.f;
	self.tableListDictrict.layer.borderColor = [UIColor lightGrayColor].CGColor;
	
	_locationService = [[LocationService alloc] init];
	[_locationService startLocationService];
	__weak typeof(self) weak = self;
	_locationService.getGeocoderComplete = ^(CLPlacemark *geo){
		if (geo) {
			BOOL correctDistrict = NO;
			for (District *districtItem in [NVCacheManager sharedInstance].listDistrict) {
				if ([districtItem.nameDistrict.lowercaseString isEqualToString:geo.subAdministrativeArea.lowercaseString]) {
					weak.districtId = districtItem.idDistrict;
					weak.tittleDistrict.text = districtItem.nameDistrict;
					[weak loadListHotel];
					correctDistrict = YES;
				}
			}
			
			if (!correctDistrict) {
				weak.tittleDistrict.text = @"NAVI";
				[weak.indicator stopAnimating];
				weak.tableListHotel.hidden = YES;
				weak.emptyLabel.hidden = NO;
			}
		}else{
			weak.tittleDistrict.text = @"NAVI";
			[weak.indicator stopAnimating];
			weak.tableListHotel.hidden = YES;
			weak.emptyLabel.hidden = NO;
		}
	};
	
    _emptyLabel.hidden = YES;
    _loadingIndicator.hidden = YES;
    _currentPage = 1;
    _totalPage = 0;
    _tableListHotel.separatorColor = [UIColor clearColor];
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self
                            action:@selector(refreshListHotel)
                  forControlEvents:UIControlEventValueChanged];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = _tableListHotel;
    tableViewController.refreshControl = _refreshControl;
	
	SWRevealViewController *revealController = [self revealViewController];
	revealController.delegate = self;
	[self.view addGestureRecognizer:revealController.panGestureRecognizer];
//	UITapGestureRecognizer *clickBgButton = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickBackFromMenu:)];
//	clickBgButton.delegate = self;
//	[self.view addGestureRecognizer:clickBgButton];
	
    self.indicator.hidden = NO;
	self.indicator.hidesWhenStopped = YES;
    [self.indicator startAnimating];
    _tittleDistrict.text = @"NAVI";

	self.tableListDictrict.hidden = YES;
    
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	// Enable swipe back
	if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
		self.navigationController.interactivePopGestureRecognizer.enabled = YES;
		self.navigationController.interactivePopGestureRecognizer.delegate = nil;
	}
    
    [self setupLanguage];
}

-(void)setupLanguage{
    self.lbEmptyHotel.text = CustomLocalisedString(@"EmptyHotel", @"The string to display in the text view.");
}


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
	return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	return ![NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"];
}
#pragma mark - IBActions

- (IBAction)clickMenuButton:(id)sender {
	[[AppDelegate sharedDelegate].viewController revealToggle:nil];
}

- (IBAction)clickDistrictFilterButton:(id)sender{
	self.tableListDictrict.hidden = !self.tableListDictrict.isHidden;
}

- (void)clickBackFromMenu:(UITapGestureRecognizer *)tapGesture{
	[self clickMenuButton:nil];
	_bgButton.hidden = YES;
    [self setupLanguage];
}

#pragma mark - Delegates

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position{
	if (position == FrontViewPositionLeft) {
		NSLog(@"close");
		_bgButton.hidden = YES;
		[self enableDictrictSelection:YES];
		self.view.alpha = 1.0f;
        [self setupLanguage];
	}else{
		_bgButton.hidden = NO;
		[self enableDictrictSelection:NO];
		self.view.alpha = 0.5f;
		NSLog(@"open");
	}
}

- (void)enableDictrictSelection:(BOOL)enable{
	self.choiceDistricButton.enabled = enable;
	self.choiceDistricButton.userInteractionEnabled = enable;
}

- (NSMutableArray *)convertJsonToListHotels:(NSMutableArray *)listHotels{
    NSMutableArray *listHotelsTempt = [NSMutableArray new];
    for (int i = 0; i < listHotels.count; i++) {
        Hotel *hotelItem = [Hotel convertJsonToHotel:listHotels[i]];
        [listHotelsTempt addObject:hotelItem];
    }
    return listHotelsTempt;
}

- (IBAction)clickBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Tableview delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (tableView == self.tableListHotel) {
		static NSString *ProjectCellIdentifier = @"ProjectCell";
		
		NVHotelViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ProjectCellIdentifier];
		
		if (cell == nil) {
			if (IS_IPHONE_6)
				cell = (NVHotelViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"NVHotelViewCellIp6" owner:self options:nil] lastObject];
			else if (IS_IPHONE_6P)
				cell = (NVHotelViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"NVHotelViewCellIp6Plus" owner:self options:nil] lastObject];
			else
				cell = (NVHotelViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"NVHotelViewCell" owner:self options:nil] lastObject];
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
		}
		//    cell.tag = indexPath.row;
		//    cell.bookNowButton.tag = indexPath.row;
		[cell setupCellWithHotelItem:_listHotels[indexPath.row]];
		//    [cell.bookNowButton addTarget:self action:@selector(clickBookNow:) forControlEvents:UIControlEventTouchUpInside];
		return cell;
	}


	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultCell"];
	if (!cell) {
		 cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DefaultCell"];
		cell.textLabel.textAlignment = NSTextAlignmentCenter;
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}
	District *dictrict = [NVCacheManager sharedInstance].listDistrict[indexPath.row];
	cell.textLabel.text = dictrict.nameDistrict;
	return cell;
}

//- (void)clickBookNow:(id)sender{
//    NVEnterBookingViewController *enterBookingVC = [[NVEnterBookingViewController alloc]init];
//    [NVCacheManager sharedInstance].lastHotel =  _listHotels[[(UIButton *)sender tag]];
//    enterBookingVC.hotelItem = _listHotels[[(UIButton *)sender tag]];
//    [self.navigationController pushViewController:enterBookingVC animated:YES];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (tableView == self.tableListHotel) {
		return _listHotels.count;
	}
	return [NVCacheManager sharedInstance].listDistrict.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (tableView == self.tableListHotel) {
		if (IS_IPHONE_6) {
			return 217;
		}else if (IS_IPHONE_6P) {
			return 240;
		}else return 200;
	}
	
	return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	if (tableView == self.tableListHotel) {
		NVDetailHotelViewController *detailHotelVC = [[NVDetailHotelViewController alloc]initWithNibName:[NVCacheManager sharedInstance].DetailHotelName bundle:nil];
		[NVCacheManager sharedInstance].lastHotel =  _listHotels[indexPath.row];
		detailHotelVC.hotelItem = _listHotels[indexPath.row];
		[self.navigationController pushViewController:detailHotelVC animated:YES];
	}
	
	if (tableView == self.tableListDictrict) {
		self.tableListDictrict.hidden = YES;
		[self.indicator startAnimating];
		
		District *selectedDictrict = [NVCacheManager sharedInstance].listDistrict[indexPath.row];
		_districtId = selectedDictrict.idDistrict;
		self.tittleDistrict.text = selectedDictrict.nameDistrict;
		[self loadListHotel];
	}
	
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
//    if (self.loadingIndicator.isAnimating || (self.listHotels.count > 0 && self.listHotels.count % 20 > 0)) {
//        // is loading --> return
//        // load at the end list --> return
//        return;
//    }
	if (scrollView == self.tableListHotel) {
		if (scrollView.contentOffset.y > scrollView.contentSize.height - CGRectGetHeight(scrollView.frame) + 30) {
			if (_currentPage < _totalPage) {
				_currentPage ++;
				[self loadListHotels];
			}
		}
	}
}

#pragma mark - Private methods

- (void)loadListHotel{
    _currentPage = 1;
    [_listHotels removeAllObjects];
    [self.tableListHotel reloadData];
    [[NetworkingManager sharedInstance]getListHotelWithDistrict:_districtId andPage:_currentPage complete:^(BOOL success, id responseObject) {
        [self.indicator stopAnimating];
         self.indicator.hidden = YES;
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        if (success) {
            NSLog(@"%@",responseObject);
            _totalPage = [responseObject[@"max_page"] integerValue];
            _listHotels = [self convertJsonToListHotels:responseObject[@"items"]];
            if (_listHotels.count == 0) {
                _emptyLabel.hidden = NO;
                _tableListHotel.hidden = YES;
			}else{
				_emptyLabel.hidden = YES;
				_tableListHotel.hidden = NO;
			}
            [self.tableListHotel reloadData];
        }else{
            if ([responseObject isKindOfClass:[NSError class]]) {
                UIAlertView *alertTimeOut = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Không thể kết nối đến server, xin vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertTimeOut show];
            }
        }
    }];
}

- (void)refreshListHotel{
    NSLog(@"refresh");
    [self loadListHotel];
}

- (void)loadListHotels{
//    [[Utils getInstance]shouldShowProgressHUD:YES WithText:@""];
    [[NetworkingManager sharedInstance]getListHotelWithDistrict:_districtId andPage:_currentPage complete:^(BOOL success, id responseObject) {
//        [[Utils getInstance]shouldShowProgressHUD:NO WithText:@""];
        if (success) {
            NSArray *listHotel = [self convertJsonToListHotels:responseObject[@"items"]];
            [_listHotels addObjectsFromArray:listHotel];
            [_tableListHotel reloadData];
        }else{
            if ([responseObject isKindOfClass:[NSError class]]) {
                UIAlertView *alertTimeOut = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Không thể kết nối đến server, xin vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertTimeOut show];
            }
        }
    }];
}

@end
