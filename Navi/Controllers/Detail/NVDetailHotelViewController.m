//
//  NVDetailHotelViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVDetailHotelViewController.h"
#import <MapKit/MapKit.h>
#import "NVEnterBookingViewController.h"
#import "NVRoomOverView.h"
#import "NVBottomHotelDetail.h"
#import "HotelDetail.h"
#import "NVRoomDetailViewController.h"
#import "NVAccommodationCell.h"
#import "Accomodation.h"
//#define HEIGHT_ROOM 250     // ip6+ 267     // ip5 210
//#define HEIGHT_HEADER 295   // ip6+ 305     // ip5 250
//#define HEIGHT_BOTTOM 360   // ip6+ 367     // ip5 330
@interface NVDetailHotelViewController () <MKMapViewDelegate,UIGestureRecognizerDelegate,RoomDetailDelegate,UICollectionViewDataSource,UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewForMap;
@property (strong, nonatomic) IBOutlet UIView *hearderView;
@property (strong, nonatomic) IBOutlet UIView *listRoomView;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *hotelName;
@property (strong, nonatomic) IBOutlet UILabel *hotelAddress;
@property (strong, nonatomic) IBOutlet UILabel *hotelStar;
@property (strong, nonatomic) IBOutlet UIImageView *hotelImage;
@property (strong, nonatomic) IBOutlet UILabel *statusBarTittle;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionAccommodation;
@property (nonatomic) NVBottomHotelDetail *bottomDetail;
@property (nonatomic) UIView *mapViewLocation;
@property (nonatomic) HotelDetail *hotelDetail;
@property (nonatomic) NSMutableArray *listRoom;
@property (weak, nonatomic) IBOutlet UIButton *tittleBUtton;
@property (nonatomic) BOOL outOfRooms;
@end

@implementation NVDetailHotelViewController{
    float headerHeight;
    float listRoomHeight;
    float roomHeight;
    float bottomViewHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    _statusBarTittle.text = _hotelItem.name;
    [_tittleBUtton setTitle:[_hotelItem.name uppercaseString] forState:UIControlStateNormal];
    if (IS_IPHONE_5) {
        roomHeight = 210;
        headerHeight = 250;
        bottomViewHeight = 330;
    }else if (IS_IPHONE_6){
        roomHeight = 250;
        headerHeight = 295;
        bottomViewHeight = 360;
    }else{
        roomHeight = 267;
        headerHeight = 305;
        bottomViewHeight = 367;
    }
//    headerHeight = HEIGHT_HEADER;
//    bottomViewHeight = HEIGHT_BOTTOM;
    _bottomDetail = [[[NSBundle mainBundle] loadNibNamed:[NVCacheManager sharedInstance].DetailHotelName_BottomView owner:self options:nil] lastObject];
    
    self.collectionAccommodation.delegate = self;
    self.collectionAccommodation.dataSource = self;
    [self.collectionAccommodation registerNib:[UINib nibWithNibName:@"NVAccommodationCell" bundle:nil] forCellWithReuseIdentifier:@"NVAccommodationCell"];
    [self.collectionAccommodation reloadData];
    
    [self.collectionAccommodation setShowsHorizontalScrollIndicator:NO];
    [self.collectionAccommodation setShowsVerticalScrollIndicator:NO];
    
    [self setupViewWithHoteItem:_hotelItem];
    [[Utils getInstance]shouldShowProgressHUD:YES WithText:@""];
    [[NetworkingManager sharedInstance] loadHotelDetailWithHotelId:_hotelItem.hotelId withDate:[self getToDay] complete:^(BOOL success, id responseObject) {
        [[Utils getInstance]shouldShowProgressHUD:NO WithText:@""];
        if (success) {
            NSLog(@"%@",responseObject);
            _hotelDetail = [HotelDetail convertJsonToHotelDetail:responseObject];
            if (!_bottomDetail.hotelDetail) {
                _bottomDetail.hotelDetail = [HotelDetail new];
            }
            _bottomDetail.hotelDetail = _hotelDetail;
            [self initLayout];
            [_bottomDetail initMapsAndDescription];
            [_collectionAccommodation reloadData];
//            if (_hotelDetail.listRoom.count == 0) {
//                [Utils showAlertWithTitle:@"Thông báo" mess:@"Hiện tại khách sạn đã hết phòng. Vui lòng quay lại sau."];
//            }
        }else{
            if ([responseObject isKindOfClass:[NSError class]]) {
                UIAlertView *alertTimeOut = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Không thể kết nối đến server, xin vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertTimeOut show];
            }else{
                UIAlertView *alertError = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Có lỗi trong quá trình tải dữ liệu. Xin vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertError show];
            }
        }
    }];
}

-(NSString *)getToDay{
    NSString *toDay;
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:usLocale];
    toDay = [formatter stringFromDate:date];
    return toDay;
}

-(void)initLayout{
    CGRect frame = _listRoomView.frame;
    frame.size.height = [self caculateListRoomHeight];
    frame.origin.y = CGRectGetMaxY(_hearderView.frame);
    _listRoomView.frame = frame;

    [self addBottomView];
    
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, [self caculateScrollHeight]);
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [self setupViewWithHoteItem:_hotelItem];
//    [[Utils getInstance]shouldShowProgressHUD:YES WithText:@""];
//    [[NetworkingManager sharedInstance]loadHotelDetailWithHotelId:1 complete:^(BOOL success, id responseObject) {
//        [[Utils getInstance]shouldShowProgressHUD:NO WithText:@""];
//        if (success) {
//            NSLog(@"%@",responseObject);
//            _hotelDetail = [HotelDetail convertJsonToHotelDetail:responseObject];
//            if (!_bottomDetail.hotelDetail) {
//                _bottomDetail.hotelDetail = [HotelDetail new];
//            }
//            _bottomDetail.hotelDetail = _hotelDetail;
//            [self initLayout];
//            [_bottomDetail initMapsAndDescription];
//        }else{
//            if ([responseObject isKindOfClass:[NSError class]]) {
//                UIAlertView *alertTimeOut = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Không thể kết nối đến server, xin vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alertTimeOut show];
//            }else{
//                UIAlertView *alertError = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Có lỗi trong quá trình tải dữ liệu. Xin vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alertError show];
//            }
//        }
//    }];
}

-(float)caculateScrollHeight{
    float totalHeight;
    listRoomHeight = _listRoomView.frame.size.height;
    bottomViewHeight = _bottomView.frame.size.height;
    totalHeight = headerHeight + listRoomHeight + bottomViewHeight;
    
    return totalHeight;
}

-(float)caculateListRoomHeight{
    float totalListRoomHeight;
    
    totalListRoomHeight = _hotelDetail.listRoom.count*roomHeight;
    
    [self addRoomToListRoom];
    return totalListRoomHeight;
}

-(void)addRoomToListRoom{
    if (_hotelDetail.listRoom.count >0) {
		NSLog(@"addRoomToListRoom");
        for (int i = 0; i < _hotelDetail.listRoom.count; i++) {
            NVRoomOverView *roomDetailItem = [[[NSBundle mainBundle] loadNibNamed:[NVCacheManager sharedInstance].DetailHotelName_RoomOverView owner:self options:nil] lastObject];
            roomDetailItem.delegate = self;
            roomDetailItem.roomDetail =_hotelDetail.listRoom[i];
            [roomDetailItem setupWithRoomDetail:_hotelDetail.listRoom[i]];
            [_listRoomView addSubview:roomDetailItem];
            CGRect frame = roomDetailItem.frame;
            frame.origin.y = i*roomHeight;
            frame.size.width = _listRoomView.frame.size.width;
            roomDetailItem.frame = frame;
        }
    }
}

-(void)addBottomView{
    CGRect frameBottom = _bottomDetail.frame;
    frameBottom.size.width = _bottomView.frame.size.width;
    _bottomDetail.frame = frameBottom;
    
    CGRect frame = _bottomView.frame;
    frame.origin.y = CGRectGetMaxY(_listRoomView.frame);
    frame.size.height = _bottomDetail.frame.size.height;
    
    _bottomView.frame = frame;
    [_bottomView addSubview:_bottomDetail];
}

-(void)setupViewWithHoteItem:(Hotel *)hotelItem{
    [_bottomDetail.hotelDescription setText:_hotelDetail.hotelDescription];
    _hotelName.text = [hotelItem.name uppercaseString];
    _hotelAddress.text = hotelItem.address;

	[Utils imageView:self.hotelImage setImageWithURL:[Utils thumbnailHotelUrl:hotelItem.image] placeholder:[UIImage imageNamed:@"hotel_default"]];
	
//    [_hotelDescription setText:hotelItem.hotelDescription];
//    [_hotelDescription setTextColor:[UIColor whiteColor]];
    NSString *starString = @"";
    switch (hotelItem.star) {
        case 1:
            starString = [NSString stringWithUTF8String:"\u2605 "];
            break;
        case 2:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 "];
            break;
        case 3:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 "];
            break;
        case 4:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 \u2605 "];
            break;
        case 5:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 \u2605 \u2605 "];
            break;
        default:
            break;
    }
    self.hotelStar.text = starString;
//    CGRect frame = self.hotelStar.frame;
//    [self.hotelStar sizeToFit];
//    frame.origin.x += (frame.size.width - self.hotelStar.frame.size.width);
//    frame.size.width = self.hotelStar.frame.size.width;
//    self.hotelStar.frame = frame;
}

- (IBAction)clickBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Room detail delegate

-(void)clickRoomDetailWithRoomId:(RoomDetail *)roomDetail{
//    NVRoomDetailViewController *roomDetailVC = [[NVRoomDetailViewController alloc]initWithNibName:[NVCacheManager sharedInstance].RoomDetailView bundle:nil];
//    roomDetailVC.roomDetail = roomDetail;
//    roomDetailVC.hotelItem = _hotelItem;
//    [self.navigationController pushViewController:roomDetailVC animated:YES];
	NVEnterBookingViewController *enterBookingVC = [[NVEnterBookingViewController alloc]initWithNibName:[NVCacheManager sharedInstance].BookingViewName bundle:nil];
	enterBookingVC.roomDetail = roomDetail;
	enterBookingVC.hotelItem = _hotelItem;
	[self.navigationController pushViewController:enterBookingVC animated:YES];

}

#pragma mark - Collectionview delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _hotelDetail.listAccommodation.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NVAccommodationCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NVAccommodationCell" forIndexPath:indexPath];
    Accomodation *acco = _hotelDetail.listAccommodation[indexPath.row];
	[Utils imageView:cell.iconImageView setImageWithURL:[Utils iconAccommodationUrl:acco.icon] placeholder:nil];
    cell.nameLabel.text = acco.name;
    if ([acco.name isEqualToString:@"Giữ xe miễn phí"]) {
        cell.nameLabel.text = @"Giữ xe \n miễn phí";
    }else if ([acco.name isEqualToString:@"Air Conditioner"]) {
        cell.nameLabel.text = @"Air\nConditioner";
    }
	return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    float width = collectionView.frame.size.width;
//    float height = collectionView.frame.size.height;
    return CGSizeMake(70,50);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


@end
