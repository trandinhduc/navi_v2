//
//  NVDetailHotelViewController.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Hotel.h"
@interface NVDetailHotelViewController : UIViewController
@property (nonatomic) Hotel *hotelItem;
-(void)setupViewWithHoteItem:(Hotel *)hotelItem;
@end
