//
//  IDSNavigationViewController.m
//  IDS
//
//  Created by Tran Dinh Duc on 8/25/15.
//  Copyright (c) 2015 TMA. All rights reserved.
//

#import "NaviNavigationViewController.h"
#import "NVHomeViewController.h"
#import "NVListHotelViewController.h"

@interface NaviNavigationViewController ()
@property (nonatomic, strong) NVHomeViewController *homeVC;
@property (nonatomic, strong) NVListHotelViewController *listHotelVC;
@end

@implementation NaviNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self performSelector:@selector(handlerData) withObject:nil afterDelay:0.2f];
}

- (NVHomeViewController *)homeVC{
    if (!_homeVC) {
        _homeVC = [[NVHomeViewController alloc] initWithNibName:@"NVHomeViewController" bundle:nil];
    }
    return _homeVC;
}

- (NVListHotelViewController *)listHotelVC{
	if (!_listHotelVC) {
		_listHotelVC = [[NVListHotelViewController alloc]initWithNibName:[NVCacheManager sharedInstance].ListHotelViewName bundle:nil];
	}
	return _listHotelVC;
}

- (void)handlerData{
//    BOOL isLogin = NO;
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if (([defaults boolForKey:USER_LOGIN] ? YES : NO)) {
        [self pushViewController:self.listHotelVC animated:YES];
//    }else{
//        [self pushViewController:self.loginVC animated:YES];
//    }
}

-(void)goHomeScreen{
    if ([self viewControllers].count > 0) {
        [self popToRootViewControllerAnimated:NO];
        UIViewController *rootVC = [self viewControllers][0];
        if ([rootVC isKindOfClass:[NVListHotelViewController class]]) {
            return;
        }else{
            [rootVC removeFromParentViewController];
            [rootVC.view removeFromSuperview];
        }
    }
    [self pushViewController:self.listHotelVC animated:YES];
}

@end
