//
//  IDSNavigationViewController.h
//  IDS
//
//  Created by Tran Dinh Duc on 8/25/15.
//  Copyright (c) 2015 TMA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NaviNavigationViewController : UINavigationController

-(void)goHomeScreen;
//-(void)goLoginScreen;
@end
