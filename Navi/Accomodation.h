//
//  Accomodation.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/9/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Accomodation : NSObject

@property (nonatomic) NSInteger idAcommodation;
@property (nonatomic) NSString *icon;
@property (nonatomic) NSString *name;
+(NSMutableArray *)convertJsonToAccommodation:(NSDictionary *)dict;

@end
