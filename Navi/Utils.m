//
//  Utils.m
//  SmartWave
//
//  Created by Si Nguyen on 3/31/15.
//  Copyright (c) 2015 Origin Agency. All rights reserved.
//

#import "Utils.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
//#import <CommonCrypto/CommonHMAC.h>

@implementation Utils

+ (Utils *) getInstance {
    
    static Utils *instance =nil;
    
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [Utils new];            
        }
    }
    return instance;
}

#pragma mark - Public Method

- (void)shouldShowProgressHUD:(BOOL)shouldShow WithText:(NSString *)progressTitle {
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithWindow:[[[UIApplication sharedApplication] delegate] window]];
        _progressHUD.delegate = self;
        _progressHUD.removeFromSuperViewOnHide = YES;
        _progressHUD.alpha = 1.0;
        [[[[UIApplication sharedApplication] delegate] window] addSubview:_progressHUD];
    }
    
    if (!progressTitle || [progressTitle isEqualToString:@""]){
        _progressHUD.labelText = nil;
    }
    else
    {
        _progressHUD.labelText = progressTitle;
    }
    
    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:_progressHUD];
    
    if (shouldShow) {
        [_progressHUD show:YES];
    }
    else
    {
        [_progressHUD hide:YES];
    }
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD was hidded
    [_progressHUD removeFromSuperview];
    _progressHUD = nil;
}

- (void)showProgressHUDSuccess {
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithWindow:[[[UIApplication sharedApplication] delegate] window]];
        _progressHUD.delegate = self;
        _progressHUD.removeFromSuperViewOnHide = YES;
        _progressHUD.alpha = 1.0;
        [[[[UIApplication sharedApplication] delegate] window] addSubview:_progressHUD];
    }
    
    _progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    _progressHUD.mode = MBProgressHUDModeCustomView;
    _progressHUD.labelText = @"Success";
    
    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:_progressHUD];
    
    [_progressHUD hide:YES afterDelay:2];
}

- (void)shouldShowProgressHUD:(BOOL)shouldShow WithProgress:(float)progress {
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithWindow:[[[UIApplication sharedApplication] delegate] window]];
        _progressHUD.delegate = self;
        _progressHUD.removeFromSuperViewOnHide = YES;
        _progressHUD.alpha = 1.0;
        _progressHUD.mode = MBProgressHUDModeAnnularDeterminate;
        _progressHUD.labelText = @"Uploading";
        [[[[UIApplication sharedApplication] delegate] window] addSubview:_progressHUD];
    }
    
    if (progress < 1.0f){
        _progressHUD.progress = progress;
    }
    
    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:_progressHUD];
    
    if (shouldShow) {
        [_progressHUD show:YES];
    }
    else
    {
        [_progressHUD hide:YES];
    }
}

- (void)updateProgressHUDWithProgress:(float)progress {
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithWindow:[[[UIApplication sharedApplication] delegate] window]];
        _progressHUD.delegate = self;
        _progressHUD.removeFromSuperViewOnHide = YES;
        _progressHUD.alpha = 1.0;
        _progressHUD.mode = MBProgressHUDModeAnnularDeterminate;
        [[[[UIApplication sharedApplication] delegate] window] addSubview:_progressHUD];
    }
    
    if (progress < 1.0f){
        _progressHUD.progress = progress;
    }
}

#pragma mark - Instance Method

+ (NSString *)deviceUUDI{
    return [[[[UIDevice currentDevice] identifierForVendor] UUIDString] copy];
}

+ (NSString *)relativeDateStringForDate:(NSDate *)date
{
    //    NSCalendarUnit units = NSDayCalendarUnit | NSWeekOfYearCalendarUnit |
    //    NSMonthCalendarUnit | NSYearCalendarUnit;
    NSCalendarUnit units = NSCalendarUnitMinute|NSCalendarUnitHour;
    
    //    // if `date` is before "now" (i.e. in the past) then the components will be positive
    //    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
    //                                                                   fromDate:date
    //                                                                     toDate:[NSDate date]
    //                                                                    options:0];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:date];
    
    //    if (components.year > 0) {
    //        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    //    } else if (components.month > 0) {
    //        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    //    } else if (components.weekOfYear > 0) {
    //        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    //    } else if (components.day > 0) {
    //        if (components.day > 1) {
    //            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
    //        } else {
    //            return @"Yesterday";
    //        }
    //    } else {
    //        return @"Today";
    //    }
    return [NSString stringWithFormat:@"%2ld:%.2ld",(long)components.hour,(long)components.minute];
}

+(NSString *)relativeTimeStringForDate:(NSDate *)date{
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date];
    
    int numberOfDays = floor(secondsBetween / 86400);
    int numberOfHours = floor((secondsBetween - numberOfDays * 86400)/3600);
    int numberOfMinutes =  floor((secondsBetween - numberOfDays * 86400 - numberOfHours*3600)/60);
    NSString *timeStr = @"";
    if (numberOfDays>0) {
        if (numberOfDays == 1) {
            timeStr = [timeStr stringByAppendingString:@"1day"];
        }else{
            timeStr = [timeStr stringByAppendingFormat:@"%dd",numberOfDays];
        }
    }
    if (numberOfHours > 0) {
        if (numberOfHours == 1) {
            timeStr = [timeStr stringByAppendingString:@" 1h"];
        }
        timeStr = [timeStr stringByAppendingFormat:@" %dhrs",numberOfHours];
    }
    
    if (numberOfMinutes > 0) {
        if (numberOfMinutes == 1) {
            timeStr = [timeStr stringByAppendingString:@" 1m"];
        }else{
            timeStr = [timeStr stringByAppendingFormat:@" %dms",numberOfMinutes];
        }
    }
    if (timeStr.length == 0) {
        timeStr = @"1m";
    }
    return timeStr;
}

+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)validatePhoneWithString:(NSString*)phone
{
    NSString *phoneRegex = @"[0-9]{6,14}";// @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phone];
}

+ (BOOL)validateUrlWithString:(NSString *)candidate {
    if (!candidate) return NO;
    if ([candidate rangeOfString:@"http://"].location != NSNotFound || [candidate rangeOfString:@"http://"].location != NSNotFound) {
        return YES;
    }
    return NO;
//    NSString *urlRegEx =
//    @"(http|https)://";
//    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
//    BOOL isValid = [urlTest evaluateWithObject:candidate];
//    return isValid;
}


+ (void) showAlertWithTitle:(NSString*)title mess:(NSString*)mess
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:mess
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

#pragma mark -
+ (CGSize)screenSize
{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
    {
        return CGSizeMake(screenSize.height, screenSize.width);
    }
    return screenSize;
}

+(UIImage*)colorImage:(UIImage *)img intoColor:(UIColor*)color{
    
    // begin a new image context, to draw our colored image onto
    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, img.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // set the blend mode to color burn, and the original image
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
    CGContextDrawImage(context, rect, img.CGImage);
    
    // set a mask that matches the shape of the image, then draw (color burn) a colored rectangle
    CGContextClipToMask(context, rect, img.CGImage);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
    
    //    [color setFill];
    //    UIRectFill(rect);   // Fill it with your color
    //    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    
    //    return image;
}

#pragma mark -
+ (BOOL)cameraIsPermission{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusAuthorized) {
        return YES;
    }else if (authStatus == AVAuthorizationStatusNotDetermined){
        return YES;
    }
    return NO;
}

+(BOOL)photoIsPermission {
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    if (status == ALAuthorizationStatusAuthorized) {
        return YES;
    }else if (status == ALAuthorizationStatusNotDetermined){
        return YES;
    }
    return NO;
}

static AVAudioPlayer *audioPlayer = nil;
+ (void)playSoundComplete:(void (^)())complete{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *filePath = [mainBundle pathForResource:@"kiss_sound" ofType:@"mp3"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    
    if (!audioPlayer) {
        audioPlayer = [[AVAudioPlayer alloc] initWithData:fileData error:nil];
        [audioPlayer prepareToPlay];
    }
    [audioPlayer play];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        complete();
    });
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (BOOL)validateYearOld:(in id)yearOld Over:(int)min{
//    NSDate *currentDate = [NSDate date];
    return YES;
}
+(NSString *)convertPriceIntegerToString:(NSString *)price{
    NSRange range = [price rangeOfString:@","];
    
    if (range.location == NSNotFound) {
        NSNumber *someNumber = [NSNumber numberWithDouble:[price doubleValue]];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
//        [nf setNumberStyle:kCFNumberFormatterDecimalStyle];
        [nf setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString *someString = [nf stringFromNumber:someNumber];
        return someString;
    }else{
        return price;
    }
}

#pragma mark -
+ (NSURL *)thumbnailHotelUrl:(NSString *)imageString{
    if ([Utils validateUrlWithString:imageString]) {
        return [NSURL URLWithString:imageString];
    }
    return [NSURL URLWithString:[[NVCacheManager sharedInstance].hotelImageUrl stringByAppendingPathComponent:imageString]];
}

+ (NSURL *)imageRoomUrl:(NSString *)imageString{
    if ([Utils validateUrlWithString:imageString]) {
        return [NSURL URLWithString:imageString];
    }
    return [NSURL URLWithString:[[NVCacheManager sharedInstance].roomImageUrl stringByAppendingPathComponent:imageString]];
}


+ (NSURL *)iconAccommodationUrl:(NSString *)iconString{
    if ([Utils validateUrlWithString:iconString]) {
        return [NSURL URLWithString:iconString];
    }
    return [NSURL URLWithString:[[NVCacheManager sharedInstance].accommodationImageUrl stringByAppendingPathComponent:iconString]];
}

+ (void)imageView:(UIImageView *)imageView setImageWithURL:(NSURL *)urlString placeholder:(UIImage *)placeholder{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
		__weak UIImageView *weakImageView = imageView;
		NSURLRequest *imageRequest = [NSURLRequest requestWithURL:urlString cachePolicy:0 timeoutInterval:150];
		[imageView setImageWithURLRequest:imageRequest placeholderImage:placeholder success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
			if (image) {
				dispatch_async(dispatch_get_main_queue(), ^{
					[weakImageView setImage:image];
				});
			}
		} failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
			
		}];
		
	});
}

+ (BOOL)isToday:(NSDate *)date{
	NSCalendar *cal = [NSCalendar currentCalendar];
	NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
	NSDate *today = [cal dateFromComponents:components];
	components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
	NSDate *otherDate = [cal dateFromComponents:components];
	
	if([today isEqualToDate:otherDate]) {
		return YES;
	}
	return NO;
}

+ (BOOL)isYesterday:(NSDate *)date{
	NSCalendar *cal = [NSCalendar currentCalendar];
	NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
	NSDate *today = [cal dateFromComponents:components];
	components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
	components.day += 1;
	NSDate *otherDate = [cal dateFromComponents:components];
	
	if([today isEqualToDate:otherDate]) {
		return YES;
	}
	return NO;
}

+ (BOOL)date:(NSDate *)date isSameDate:(NSDate *)date2{
	NSCalendar *cal = [NSCalendar currentCalendar];
	NSDateComponents *components = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
	
	NSDateComponents *otherComponents = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date2];
	
	if(components.day == otherComponents.day && components.month == otherComponents.month && components.year == otherComponents.year) {
		return YES;
	}
	return NO;
}


+ (NSString *)getLocaleString{
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    Locale *selectedLocale = [languageManager getSelectedLocale];
    return selectedLocale.languageCode;
}

@end