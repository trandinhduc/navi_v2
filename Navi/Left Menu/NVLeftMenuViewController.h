//
//  NVLeftMenuViewController.h
//  Navi
//
//  Created by Tran Dinh Duc on 11/24/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NVLeftMenuAction <NSObject>

-(void)clickAboutButton;
-(void)clickSupportButton;
@end

@interface NVLeftMenuViewController : UIViewController

@property (nonatomic, weak) id<NVLeftMenuAction> delegate;

@end
