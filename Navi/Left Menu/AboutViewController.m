//
//  AboutViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/7/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()
@property (strong, nonatomic) IBOutlet UITextView *naviDescription;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_naviDescription setText:[NVCacheManager sharedInstance].naviDesciption];
    [_backButton setTitle:CustomLocalisedString(@"AboutTittle", @"About") forState:UIControlStateNormal];
}

- (IBAction)backButtonDidTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
