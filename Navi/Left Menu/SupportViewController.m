//
//  SupportViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/7/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "SupportViewController.h"
#import "NVCacheManager.h"
@interface SupportViewController ()
- (IBAction)clickSendMailButton:(UIButton *)sender;

@end

@implementation SupportViewController{
    MFMailComposeViewController *mc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_backButton setTitle:CustomLocalisedString(@"SupportTittle", @"Support") forState:UIControlStateNormal];
    [_sendMailButton setTitle:CustomLocalisedString(@"SendEmail", @"SendEmail") forState:UIControlStateNormal];
    
    _supportLabel.text =CustomLocalisedString(@"SupportLabel", @"SupportLabel");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonDidTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSendMailButton:(UIButton *)sender {
    if (!mc) {
        mc = [[MFMailComposeViewController alloc] init];
    }
    
    if ([MFMailComposeViewController canSendMail]) {
//        NSString *emailTitle = @"Feedback about Navi";
        // Email Content
//        NSString *messageBody = [UserProfile sharedInstance].emailBody;
        // To address
        NSString *email = [NVCacheManager sharedInstance].supportEmail;
        NSArray *toRecipents = @[email];
        
        mc.mailComposeDelegate = self;
//        [mc setSubject:emailTitle];
//        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }else{
        UIAlertView *alertCannotSend = [[UIAlertView alloc]
                                        initWithTitle:@"No Mail Accounts"
                                        message:@"Please set up a Mail account in order to send email."
                                        delegate:self
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil, nil];
        [alertCannotSend show];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [mc dismissViewControllerAnimated:YES completion:NULL];
    mc = nil;
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    NSLog(@"Finish");
//    [self.parentMessageBump dismissViewControllerAnimated:YES completion:nil];
}

@end
