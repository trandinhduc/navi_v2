//
//  NVLeftMenuViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 11/24/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVLeftMenuViewController.h"
#import "SupportViewController.h"
#import "AboutViewController.h"

@interface NVLeftMenuViewController ()

@property (weak, nonatomic) IBOutlet UIButton *supportButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutButton;
@property (weak, nonatomic) IBOutlet UILabel *tittleMenu;
@property (weak, nonatomic) IBOutlet UISwitch *swChangeLanguage;

@end

@implementation NVLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_5) {
        [_tittleMenu setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:18]];
        [_supportButton.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:16]];
        [_aboutButton.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:16]];
    }else if (IS_IPHONE_6){
        [_tittleMenu setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:18]];
        [_supportButton.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:18]];
        [_aboutButton.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:18]];
    }
    else if (IS_IPHONE_6P){
        [_tittleMenu setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:22]];
        [_supportButton.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:21]];
        [_aboutButton.titleLabel setFont:[UIFont fontWithName:@"SourceSansPro-Regular" size:21]];
    }
    if ([[Utils getLocaleString]  isEqual: @"vi"]) {
        self.swChangeLanguage.on = NO;
    }else{
        self.swChangeLanguage.on = YES;
    }
    
    [self.swChangeLanguage addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [self setupLanguage];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)changeSwitch:(id)sender{
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    if([sender isOn]){
        [languageManager setLanguageWithLocale:languageManager.availableLocales[1]];
    } else{
        [languageManager setLanguageWithLocale:languageManager.availableLocales[0]];
    }
    [self setupLanguage];
}

-(void)setupLanguage{
    [self.supportButton setTitle:CustomLocalisedString(@"SupportTittle", @"Support") forState:UIControlStateNormal];
    [self.aboutButton setTitle:CustomLocalisedString(@"AboutTittle", @"About") forState:UIControlStateNormal];
}


- (IBAction)clickSupportButton:(UIButton *)sender {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(clickSupportButton)]) {
//        [self.delegate clickSupportButton];
//    }
    SupportViewController *supportVC = [[SupportViewController alloc]initWithNibName:[NVCacheManager sharedInstance].SupportViewName bundle:nil];
    [[[self.navigationController parentViewController] navigationController] pushViewController:supportVC animated:YES];
    //    [self presentViewController:supportVC animated:YES completion:nil];
}

- (IBAction)clickAboutButton:(UIButton *)sender {
//    if (self.delegate && [self.delegate respondsToSelector:@selector(clickAboutButton)]) {
//        [self.delegate clickAboutButton];
//    }
    AboutViewController *aboutVC = [[AboutViewController alloc]initWithNibName:[NVCacheManager sharedInstance].AboutViewName bundle:nil];
    [[[self.navigationController parentViewController] navigationController] pushViewController:aboutVC animated:YES];
//    [self.navigationController pushViewController:aboutVC animated:YES];
//    [self presentViewController:aboutVC animated:YES completion:nil];

}
@end
