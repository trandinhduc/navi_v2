//
//  AboutViewController.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/7/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end
