//
//  NVSplashScreenController.m
//  Navi
//
//  Created by Tran Dinh Duc on 11/30/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVSplashScreenController.h"
#import "NVHomeViewController.h"
#import "SWRevealViewController.h"
#import "District.h"
#import "Accomodation.h"
#import "NVListHotelViewController.h"

@interface NVSplashScreenController ()

@end

@implementation NVSplashScreenController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (IS_IPHONE_5) {
        NSLog(@"iphone 5");
    }else if (IS_IPHONE_6){
        NSLog(@"iphone 6");
    }else if (IS_IPHONE_6P){
        NSLog(@"Iphone 6+");
    }
	[[NVCacheManager sharedInstance] HomeNibName];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [[Utils getInstance]shouldShowProgressHUD:YES WithText:@"Loading..."];
    [[NetworkingManager sharedInstance]loadConfigFromServer:^(BOOL success, id responseObject) {
//        [[Utils getInstance]shouldShowProgressHUD:NO WithText:@""];
        if (success) {
            NSLog(@"%@",responseObject);
           [NVCacheManager sharedInstance].listDistrict = [District convertListDistrict:responseObject[@"districts"]];
            [NVCacheManager sharedInstance].listAccommodations = [Accomodation convertJsonToAccommodation:responseObject[@"accommodations"]];
            [NVCacheManager sharedInstance].naviDesciption = responseObject[@"introduction"];
            [NVCacheManager sharedInstance].hotelImageUrl = responseObject[@"hotelImageUrl"];
            [NVCacheManager sharedInstance].roomImageUrl = responseObject[@"roomImageUrl"];
            if (![NVCacheManager sharedInstance].roomImageUrl) {
                [NVCacheManager sharedInstance].roomImageUrl = @"http://navi.geekup.vn/uploads/images/rooms/";
            }
             [NVCacheManager sharedInstance].accommodationImageUrl = responseObject[@"accommodationImageUrl"];
            [NVCacheManager sharedInstance].supportEmail = responseObject[@"supportEmail"];
			//123Pay config
			NSDictionary *dict123Pay = responseObject[@"123pay"];
			if (dict123Pay) {
				if (dict123Pay[@"create_order_url"]) {
					urlCreateOrder123Pay = dict123Pay[@"create_order_url"];
				}
				if (dict123Pay[@"query_order_url"]) {
					urlQueryOrder123Pay = dict123Pay[@"query_order_url"];
				}
				if (dict123Pay[@"merchant_code"]) {
					merchantCode = dict123Pay[@"merchant_code"];
				}
				if (dict123Pay[@"secret_key"]) {
					secrectCode = dict123Pay[@"secret_key"];
				}
				if (dict123Pay[@"passcode"]) {
					passcode = dict123Pay[@"passcode"];
				}
				if (dict123Pay[@"client_ip"]) {
					clientIp = dict123Pay[@"client_ip"];
				}
			}

            NVListHotelViewController *frontViewController = [[NVListHotelViewController alloc] initWithNibName:[NVCacheManager sharedInstance].ListHotelViewName bundle:nil];
            SWRevealViewController *revealController = [self revealViewController];
            
            UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
            frontNavigationController.navigationBarHidden = YES;
            
            [revealController setFrontViewController:frontNavigationController animated:YES];
        }
    }];
}

@end
