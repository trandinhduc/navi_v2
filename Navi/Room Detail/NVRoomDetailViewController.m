//
//  NVRoomDetailViewController.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/3/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVRoomDetailViewController.h"
#import "NVRoomDetailInformation.h"
#import "NVCollectionAccommodation.h"
#import "NVRoomImageCell.h"
#import "NVEnterBookingViewController.h"
//#define HEIGHT_INFORMATION_VIEW 220 // ip6+ 208     // ip5 168
//#define HEIGHT_HEADER 290           // ip6+ 305     // ip5 250
//#define HEIGHT_BOTTOM 280           // ip6+ 283     // ip5 274
//#define HEIGHT_BOTTOM_BUTTON 54     // ip6+         // ip5 54
@interface NVRoomDetailViewController () <UICollectionViewDataSource,UICollectionViewDelegate, NVCollectionAccommodationDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionImage;
- (IBAction)clickBackButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UILabel *tittleHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) NVRoomDetailInformation *roomDetailView;
@property (nonatomic) NVCollectionAccommodation *collectionAccommodation;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *stausBarTittle;
@property (strong, nonatomic) IBOutlet UIImageView *imageHotel;
@property (strong, nonatomic) IBOutlet UILabel *firstPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *nextPriceLabel;
- (IBAction)clickBookButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *tittleButton;

@property (weak, nonatomic) IBOutlet UIButton *orderButton;

@end

@implementation NVRoomDetailViewController{
    float informationHeight;
    float headerHeight;
    float bottomViewHeight;
    float bottomButtonHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_5) {
        informationHeight = 168;
        headerHeight = 250;
        bottomViewHeight = 274;
        bottomButtonHeight = 54;
    }else if (IS_IPHONE_6){
        informationHeight = 220;
        headerHeight = 290;
        bottomViewHeight = 280;
        bottomButtonHeight = 54;
    }else{
        informationHeight = 208;
        headerHeight = 305;
        bottomViewHeight = 283;
        bottomButtonHeight = 65;
    }
    
    [_tittleButton setTitle:_roomDetail.name forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
	[Utils imageView:self.imageHotel setImageWithURL:[Utils imageRoomUrl:_roomDetail.images[0]] placeholder:[UIImage imageNamed:@"hotel_default"]];
	
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, [self caculateHeightForView]);
    
    self.firstPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",[[[_roomDetail.prices sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
		return [obj1 integerValue] < [obj2 integerValue];
	}] firstObject] integerValue]]]];
    self.nextPriceLabel.text = [NSString stringWithFormat:@"%@ Đ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",[[[_roomDetail.prices sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
		return [obj1 integerValue] < [obj2 integerValue];
	}] objectAtIndex:1] integerValue]]]];
 
//    [self.collectionAccommodation.collectionAccommodation registerNib:[UINib nibWithNibName:@"NVRoomImageCell" bundle:nil] forCellWithReuseIdentifier:@"NVRoomImageCell"];
    
    [self generalLayout];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.collectionImage.delegate = self;
    self.collectionImage.dataSource = self;
    [self.collectionImage reloadData];
    self.collectionAccommodation.delegate = self;
    
    float insetAlpha = CGRectGetWidth(self.collectionImage.bounds) - _roomDetail.images.count*60;
    if (insetAlpha > 0) {
        self.collectionImage.contentInset = UIEdgeInsetsMake(0, insetAlpha/2, 0, insetAlpha/2);
    }else{
        self.collectionImage.contentInset = UIEdgeInsetsZero;
    }
    
    [_collectionAccommodation setItems:_roomDetail.accomodations];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.collectionImage registerNib:[UINib nibWithNibName:@"NVRoomImageCell" bundle:nil] forCellWithReuseIdentifier:@"NVRoomImageCell"];

    

//    float insetAlpha = CGRectGetWidth(self.collectionImage.bounds) - _roomDetail.images.count*70;
//    if (insetAlpha > 0) {
//        self.collectionImage.contentInset = UIEdgeInsetsMake(0, insetAlpha/2, 0, insetAlpha/2);
//    }else{
//        self.collectionImage.contentInset = UIEdgeInsetsZero;
//    }
//    [self.collectionImage reloadData];
}

-(float)caculateHeightForView{
    float totalHeight;
    
    totalHeight = bottomViewHeight+headerHeight+informationHeight;
    
    return totalHeight;
}

-(void)generalLayout{
    _roomDetailView = [[[NSBundle mainBundle] loadNibNamed:[NVCacheManager sharedInstance].RoomDetailView_Information owner:self options:nil] lastObject];
    CGRect frameDetailView = _roomDetailView.frame;
    frameDetailView.origin.y = CGRectGetMaxY(_headerView.frame);
    _roomDetailView.frame = frameDetailView;
    [_scrollView addSubview:_roomDetailView];
	
	[_roomDetailView updateInfo:_roomDetail];
    
    _collectionAccommodation =[[[NSBundle mainBundle] loadNibNamed:[NVCacheManager sharedInstance].RoomDetailView_Accomodation owner:self options:nil] lastObject];
    CGRect frameCollectionView = _collectionAccommodation.frame;
    frameCollectionView.origin.y = CGRectGetMaxY(_roomDetailView.frame);
    _collectionAccommodation.frame = frameCollectionView;
    [_scrollView addSubview:_collectionAccommodation];
}

- (IBAction)clickBackButton:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Collectionview delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _roomDetail.images.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _collectionImage) {
        NVRoomImageCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"NVRoomImageCell" forIndexPath:indexPath];
        NSString *urlImage = _roomDetail.images[indexPath.row];
		[Utils imageView:cell.image setImageWithURL:[Utils imageRoomUrl:urlImage] placeholder:[UIImage imageNamed:@"hotel_default"]];

        return cell;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(40, 40);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 12;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *urlImage = _roomDetail.images[indexPath.row];
	[Utils imageView:self.imageHotel setImageWithURL:[Utils imageRoomUrl:urlImage] placeholder:[UIImage imageNamed:@"hotel_default"]];
}

- (IBAction)clickBookButton:(UIButton *)sender {
    NVEnterBookingViewController *enterBookingVC = [[NVEnterBookingViewController alloc]initWithNibName:[NVCacheManager sharedInstance].BookingViewName bundle:nil];
    enterBookingVC.roomDetail = _roomDetail;
    enterBookingVC.hotelItem = _hotelItem;
    [self.navigationController pushViewController:enterBookingVC animated:YES];
}

#pragma mark - 
- (void)accomodationViewUpdateedHeight:(float)hei{
    self.scrollView.contentSize = CGSizeMake(1, CGRectGetMaxY(self.collectionAccommodation.frame) + 40);
}
@end
