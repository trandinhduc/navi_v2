//
//  LocationService.m
//  FiBu
//
//  Created by thinhvoxuan on 7/3/15.
//  Copyright (c) 2015 Tran Dinh Duc. All rights reserved.
//

#import "LocationService.h"

@interface LocationService(){
	CLLocationManager *locationManager;
	CLLocation *currentLocation;
}

@end

@implementation LocationService

- (instancetype)init{
    self = [super init];
    if (self) {
        [self initLocationManager];
    }
    return self;

}

- (void) startLocationService {
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

-(BOOL)checkLocation{
    if (_currentStatus == kCLAuthorizationStatusNotDetermined) {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
    }
    if (_currentStatus == kCLAuthorizationStatusDenied) {
        return NO;
    }
    return YES;
}

- (void) initLocationManager {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest; // 100 m
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    currentLocation = newLocation;
}


- (double) getLaTitute {
    if(locationManager.location){
        return locationManager.location.coordinate.latitude;
    }
    return 0.0f;
}

- (double) getLongTitude {
    if(locationManager.location){
        return locationManager.location.coordinate.longitude;;
    }
    return 0.0f;
}

- (CLLocationCoordinate2D) currentLocation {
    return locationManager.location.coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    _currentStatus = status;
    _isCheckStatus = YES;
	
	if (status == kCLAuthorizationStatusDenied) {
		[Utils showAlertWithTitle:@"Lỗi" mess:@"Vui lòng bật tính năng định vị để sử dụng Navi."];
		if (_getGeocoderComplete) {
			_getGeocoderComplete(nil);
		}
	}
	
    if (status == kCLAuthorizationStatusNotDetermined) {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
    }
    
    if (status >= kCLAuthorizationStatusAuthorizedWhenInUse) {
        [locationManager startUpdatingLocation];
		CLGeocoder *geocoder = [[CLGeocoder alloc] init];
		
		[geocoder reverseGeocodeLocation:locationManager.location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
						   dispatch_async(dispatch_get_main_queue(),^ {
							   // do stuff with placemarks on the main thread

							   if (placemarks.count == 1) {
								   CLPlacemark *place = [placemarks objectAtIndex:0];
								   NSLog(@"PLACE:%@ : \n%@",place.name, place.addressDictionary);
								   if (_getGeocoderComplete) {
									   _getGeocoderComplete(place);
								   }
							   }else{
								   if (_getGeocoderComplete) {
									   _getGeocoderComplete(nil);
								   }
							   }
							   
						   });

		}];
//		[geocoder reverseGeocodeLocation:self.locationManager.location // You can pass aLocation here instead
//					   completionHandler:^(NSArray *placemarks, NSError *error) {
//						   
//						   dispatch_async(dispatch_get_main_queue(),^ {
//							   // do stuff with placemarks on the main thread
//							   
//							   if (placemarks.count == 1) {
//								   
//								   CLPlacemark *place = [placemarks objectAtIndex:0];
//								   
//								   
//								   NSString *zipString = [place.addressDictionary valueForKey:@"ZIP"];
//								   
//								   [self performSelectorInBackground:@selector(showWeatherFor:) withObject:zipString];
//								   
//							   }
//							   
//						   });
//					   }];
//		

    }
}


//// this delegate is called when the reverseGeocoder finds a placemark
//- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
//{
//	MKPlacemark * myPlacemark = placemark;
//	// with the placemark you can now retrieve the city name
//	NSString *city = [myPlacemark.addressDictionary objectForKey:(NSString*) kABPersonAddressCityKey];
//}
//
//// this delegate is called when the reversegeocoder fails to find a placemark
//- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error
//{
//	NSLog(@"reverseGeocoder:%@ didFailWithError:%@", geocoder, error);
//}



@end
