//
//  Utils.h
//  SmartWave
//
//  Created by Si Nguyen on 3/31/15.
//  Copyright (c) 2015 Origin Agency. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
// ----------------------

//#define IS_IPHONE_4 ((fabs((double) [[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON) && isPhone())
//#define IS_IPHONE_5 ((fabs((double) [[UIScreen mainScreen] bounds].size.height - (double)568 ) < DBL_EPSILON ) && isPhone())
//#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
//
//#define IS_IPHONE_6PLUS (isPhone() && [[UIScreen mainScreen] respondsToSelector:@selector(nativeScale)] && [[UIScreen mainScreen] nativeScale] == 3.0f)

#define DEVICE_HEIGHT [UIScreen mainScreen].bounds.size.height

#define DEVICE_WIDTH [UIScreen mainScreen].bounds.size.width

#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]


@interface Utils : NSObject<MBProgressHUDDelegate>

@property (strong, nonatomic) MBProgressHUD *progressHUD;

+ (Utils*) getInstance;

- (void)shouldShowProgressHUD:(BOOL)shouldShow WithText:(NSString *)progressTitle;
- (void)showProgressHUDSuccess;

- (void)shouldShowProgressHUD:(BOOL)shouldShow WithProgress:(float)progress;
- (void)updateProgressHUDWithProgress:(float)progress;

+ (NSString *) deviceUUDI;
+ (BOOL)validateUrlWithString:(NSString *)candidate;
+ (BOOL)validateEmailWithString:(NSString*)email;
+ (BOOL)validatePhoneWithString:(NSString*)phone;
+ (void) showAlertWithTitle:(NSString*)title mess:(NSString*)mess;
+ (NSString *)relativeDateStringForDate:(NSDate *)date;
+ (NSString *)relativeTimeStringForDate:(NSDate *)date;

//+ (void)updateProfile:(NSDictionary *)data;
//+ (void)updateContact:(NSDictionary *)data;
//+ (void)updatePreference:(NSDictionary *)data;
//+ (void)updateSetting:(NSDictionary *)data;
//+ (void)updatePicture:(NSDictionary *)data position:(NSInteger)position;

+ (CGSize)screenSize;
+ (UIImage*)colorImage:(UIImage *)img intoColor:(UIColor*)color;

+ (BOOL)cameraIsPermission;
+ (BOOL)photoIsPermission;

+ (void)playSoundComplete:(void(^)())complete;
//+(UIColor*)colorWithHexString:(NSString*)hex;

+ (UIImage *)imageWithColor:(UIColor *)color;

+ (BOOL)validateYearOld:(in)yearOld Over:(int)min;

+ (BOOL)isToday:(NSDate *)date;
+ (BOOL)isYesterday:(NSDate *)date;
+ (BOOL)date:(NSDate *)date isSameDate:(NSDate *)date2;

+ (NSString *)convertPriceIntegerToString:(NSString *)price;

+ (NSURL *)thumbnailHotelUrl:(NSString *)imageString;
+ (NSURL *)imageRoomUrl:(NSString *)imageString;
+ (NSURL *)iconAccommodationUrl:(NSString *)iconString;
+ (void)imageView:(UIImageView *)imageView setImageWithURL:(NSURL *)urlString placeholder:(UIImage *)placeholder;
+ (NSString *)getLocaleString;
@end
