//
//  HotelDetail.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelDetail : NSObject
@property (nonatomic) NSString *hotelName;
@property (nonatomic) NSString *hotelDescription;
@property (nonatomic) NSMutableArray *listRoom;
@property (nonatomic) NSMutableArray *listAccommodation;
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSString *longitude;

+(HotelDetail *)convertJsonToHotelDetail:(NSDictionary *)json;
@end
