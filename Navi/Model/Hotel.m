//
//  Hotel.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "Hotel.h"

@implementation Hotel

+(Hotel *)convertJsonToHotel:(NSDictionary *)jsonHotel{
    Hotel *hotelItem = [Hotel new];
    
    hotelItem.hotelId = [jsonHotel[@"id"] integerValue];
    hotelItem.name = jsonHotel[@"name"];
    hotelItem.address = jsonHotel[@"address"];
    hotelItem.star = [jsonHotel[@"star"]integerValue];
	if (jsonHotel[@"image"]) {
		if ([jsonHotel[@"image"] isKindOfClass:[NSString class]]) {
			hotelItem.image = jsonHotel[@"image"];
		}else if ([jsonHotel[@"image"] isKindOfClass:[NSArray class]]){
			hotelItem.image = [jsonHotel[@"image"] lastObject];
		}
	}

//    hotelItem.price = jsonHotel[@"price"];
    NSString *price = [NSString stringWithFormat:@"%@",jsonHotel[@"price"]];
    hotelItem.price = [Utils convertPriceIntegerToString:price];
//    hotelItem.longitude = jsonHotel[@"longitude"];
//    hotelItem.latitude = jsonHotel[@"latitude"];
    return hotelItem;
}

+(NSDictionary *)convertHotelToDictionary:(Hotel *)hotelItem{
    NSDictionary *dict = @{
                              @"id":@(hotelItem.hotelId),
                              @"name":hotelItem.name,
                              @"address":hotelItem.address,
//                              @"description":hotelItem.hotelDescription,
                              @"star":@(hotelItem.star),
                              @"image":hotelItem.image,
                              @"price":hotelItem.price
//                              @"longitude":hotelItem.longitude,
//                              @"latitude":hotelItem.latitude
                             };
    return dict;
}

@end
