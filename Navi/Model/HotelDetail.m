//
//  HotelDetail.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "HotelDetail.h"
#import "RoomDetail.h"
#import "Accomodation.h"

@implementation HotelDetail

+(HotelDetail *)convertJsonToHotelDetail:(NSDictionary *)json{
    HotelDetail *hotelDetailItem = [HotelDetail new];
    
    hotelDetailItem.hotelName = json[@"name"];
    hotelDetailItem.hotelDescription = json[@"description"];
    hotelDetailItem.listRoom = [self convertJsonToListRoom:json[@"rooms"]];
    NSArray *accommodations = json[@"accommodations"];
    if (accommodations && [accommodations isKindOfClass:[NSArray class]] && accommodations.count > 0) {
        for (Accomodation *acc in [NVCacheManager sharedInstance].listAccommodations) {
            if ([accommodations containsObject:[NSString stringWithFormat:@"%ld",(long)acc.idAcommodation]]) {
                [hotelDetailItem.listAccommodation addObject:acc];
            }
        }
    }

    hotelDetailItem.longitude = json[@"longitude"];
    hotelDetailItem.latitude = json[@"latitude"];
    return hotelDetailItem;
}


+(NSMutableArray *)convertJsonToListRoom:(NSDictionary *)json{
    NSMutableArray *listRoom = [NSMutableArray new];
    
    for (NSDictionary *dictItem in json) {
        RoomDetail *roomItem = [RoomDetail new];
        roomItem.roomId = [dictItem[@"id"]integerValue];
        roomItem.name = dictItem[@"name"];
        roomItem.area = dictItem[@"area"];
        roomItem.bedType = dictItem[@"bedType"];
        roomItem.bathtub = [dictItem[@"bathTub"]integerValue];
        roomItem.standingBathtub = [dictItem[@"hasStandingBathTub"]integerValue];
        roomItem.prices = dictItem[@"prices"];
        roomItem.listPrice = [RoomDetail convertJsonToListPrice:dictItem[@"price_list"]];
        NSArray *accommodations = dictItem[@"accommodations"];
        if (accommodations && [accommodations isKindOfClass:[NSArray class]] && accommodations.count > 0) {
            NSMutableArray *accos = [NSMutableArray new];
            for (Accomodation *acc in [NVCacheManager sharedInstance].listAccommodations) {
                if ([accommodations containsObject:[NSString stringWithFormat:@"%ld",(long)acc.idAcommodation]]) {
                    [accos addObject:acc];
                }
            }
            roomItem.accomodations = accos;
        }

//        roomItem.accomodations = dictItem[@"accomodations"];
        roomItem.images = dictItem[@"images"];
        [listRoom addObject:roomItem];
    }
    return listRoom;
}

- (NSMutableArray *)listAccommodation{
    if (!_listAccommodation) {
        _listAccommodation = [NSMutableArray new];
    }
    return _listAccommodation;
}

- (NSMutableArray *)listRoom{
    if (!_listRoom) {
        _listRoom = [NSMutableArray new];
    }
    return _listRoom;
}
@end
