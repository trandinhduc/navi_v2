//
//  Hotel.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hotel : NSObject

@property (nonatomic) NSInteger hotelId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *hotelDescription;
@property (nonatomic) NSString *price;
@property (nonatomic) NSString *image;
@property (nonatomic) NSInteger star;
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSString *longitude;

+(Hotel *)convertJsonToHotel:(NSDictionary *)jsonHotel;
+(NSDictionary *)convertHotelToDictionary:(Hotel *)hotelItem;
@end
