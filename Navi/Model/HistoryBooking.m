//
//  HIstoryBooking.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/15/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "HistoryBooking.h"

@implementation HistoryBooking

+ (HistoryBooking *)convertHistoryBookingFromJson:(NSDictionary *)dict{
    HistoryBooking *historyBookingItem = [HistoryBooking new];
    if (dict) {
        historyBookingItem.amount = [dict[@"amount"] integerValue];
        historyBookingItem.checkIn = dict[@"checkin"];
        historyBookingItem.district = [dict[@"district"] integerValue];
        historyBookingItem.name = dict[@"hotelName"];
        historyBookingItem.bookingId = [dict[@"id"] integerValue];
        historyBookingItem.status = [dict[@"status"] integerValue];
        historyBookingItem.time = [dict[@"time"] integerValue];
    }
    return historyBookingItem;
}

@end
