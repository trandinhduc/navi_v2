//
//  RoomDetail.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomDetail : NSObject

@property (nonatomic) NSInteger roomId;
@property (nonatomic) NSString  *name;
@property (nonatomic) NSString  *area;
@property (nonatomic) NSString  *bedType;
@property (nonatomic) NSInteger bathtub;
@property (nonatomic) NSInteger standingBathtub;
@property (nonatomic) NSArray   *prices;
@property (nonatomic) NSArray   *accomodations;
@property (nonatomic) NSArray   *images;
@property (nonatomic) NSMutableDictionary *listPrice;
+(RoomDetail *)convertJsonToRoomDetail:(NSDictionary *)json;
+(NSMutableDictionary *)convertJsonToListPrice:(NSMutableDictionary *)arrayDict;
@end
