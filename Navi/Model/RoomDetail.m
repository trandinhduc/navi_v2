//
//  RoomDetail.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "RoomDetail.h"
#import "Accomodation.h"

@implementation RoomDetail

+(RoomDetail *)convertJsonToRoomDetail:(NSDictionary *)json{
    RoomDetail *roomDetail = [RoomDetail new];
    
    roomDetail.roomId = [json[@"id"]integerValue];
    roomDetail.name = json[@"name"];
    roomDetail.area = json[@"area"];
    roomDetail.bedType = json[@"bedType"];
    roomDetail.bathtub = [json[@"bathTub"]integerValue];
    roomDetail.standingBathtub = [json[@"hasStandingBathTub"]integerValue];
    roomDetail.prices = json[@"prices"];
    
    NSArray *accommodations = json[@"accommodations"];
    if (accommodations && [accommodations isKindOfClass:[NSArray class]] && accommodations.count > 0) {
        NSMutableArray *accos = [NSMutableArray new];
        for (Accomodation *acc in [NVCacheManager sharedInstance].listAccommodations) {
            if ([accommodations containsObject:[NSString stringWithFormat:@"%ld",(long)acc.idAcommodation]]) {
                [accos addObject:acc];
            }
        }
        roomDetail.accomodations = accos;
    }

    roomDetail.images = json[@"images"];
    
    return roomDetail;
}

+(NSMutableDictionary *)convertJsonToListPrice:(NSMutableDictionary *)arrayListHour{
    NSMutableDictionary *listPrice = [ NSMutableDictionary new];
    NSMutableDictionary *tempt = [arrayListHour mutableCopy];
    
    for (NSDictionary *item in tempt) {
        NSLog(@"%@",item);
    }
    
    for (NSString *day in [tempt allKeys]) {
        NSString *dayTemp = [NSString stringWithFormat:@"%@",day];
        NSLog(@"%@",day);
        NSDictionary *listHourInDay = [tempt valueForKey:dayTemp];
        NSLog(@"%@",listHourInDay);
        NSMutableDictionary *listHourInDayTemp = [NSMutableDictionary new];
        for (NSDictionary *dict in listHourInDay) {
           
            for (NSString *key in [dict allKeys]) {
                if (key.length >0) {
                    NSArray *listHour = [key componentsSeparatedByString:@"_"];
                    
                    for (NSString *hour in listHour) {
                        [listHourInDayTemp setValue:[dict valueForKey:key] forKey:hour];
                    }
                    
                }
                
            }
           
            
        }
        [listPrice setValue:listHourInDayTemp forKey:dayTemp];
    }
//    NSArray *allKeys = [arrayDict allKeys];
//    for (NSString *key in allKeys) {
//        NSDictionary *dictHour = [arrayDict valueForKey:key];
//        NSArray *listValueHour = [dictHour allKeys];
//        for (NSString *key in listValueHour) {
//            if (key.length >0) {
//                NSArray *listHour = [key componentsSeparatedByString:@"_"];
//                for (NSString *hour in listHour) {
//                        [listPrice setValue:key forKey:hour];
//                }
//            }
//        }
    
//        NSLog(@"%@",[item allKeys]);
//    }
    return listPrice;
}

@end
