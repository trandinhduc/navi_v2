//
//  HIstoryBooking.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/15/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryBooking : NSObject

@property (nonatomic) NSInteger amount;
@property (nonatomic) NSInteger bookingId;
@property (nonatomic) NSInteger district;
@property (nonatomic) NSInteger status;
@property (nonatomic) NSInteger time;
@property (nonatomic) NSString *checkIn;
@property (nonatomic) NSString *name;

+ (HistoryBooking *)convertHistoryBookingFromJson:(NSDictionary *)dict;

@end
