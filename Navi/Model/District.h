//
//  District.h
//  Navi
//
//  Created by Tran Dinh Duc on 11/30/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface District : NSObject

@property (nonatomic) NSInteger idDistrict;
@property (nonatomic) NSString *nameDistrict;
+(NSMutableArray *)convertListDistrict:(NSArray *)array;
@end
