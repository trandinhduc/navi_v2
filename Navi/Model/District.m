//
//  District.m
//  Navi
//
//  Created by Tran Dinh Duc on 11/30/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "District.h"

@implementation District

+(NSMutableArray *)convertListDistrict:(NSArray *)array{
    NSMutableArray *listDistrict = [NSMutableArray new];
//    listDistrict = [array mutableCopy];
    for (NSDictionary *dictItem in array) {
        District *districtItem = [District new];
        districtItem.idDistrict = [dictItem[@"id"] integerValue];
        districtItem.nameDistrict = dictItem[@"name"];
        [listDistrict addObject:districtItem];
    }
//    NSLog(@"%@",array);
    
//    District *districtItem = [District new];
//    districtItem.idDistrict = 1;
//    districtItem.nameDistrict = @"Quận 1";
//    [listDistrict addObject:districtItem];

    return listDistrict;
}
@end
