//
//  Accomodation.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/9/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "Accomodation.h"

@implementation Accomodation

+(NSMutableArray *)convertJsonToAccommodation:(NSDictionary *)dict{
    NSMutableArray *listAccommodation = [NSMutableArray new];
    for (NSDictionary *dictItem in dict) {
        Accomodation *accommodationItem = [Accomodation new];
        accommodationItem.idAcommodation = [dictItem[@"id"] integerValue];
        accommodationItem.name = dictItem[@"name"];
        accommodationItem.icon = dictItem[@"icon"];
        [listAccommodation addObject:accommodationItem];
    }
    return listAccommodation;
}
@end
