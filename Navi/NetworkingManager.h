//
//  NetworkingManager.h
//  FiBu
//
//  Created by An Nguyen on 7/8/15.
//  Copyright (c) 2015 Tran Dinh Duc. All rights reserved.
//

#import "DOSingleton.h"

@interface NetworkingManager : DOSingleton

- (NSString *) genUrlFromString: (NSString *) path;
// New API

-(void)loadConfigFromServer:(void(^)(BOOL success, id responseObject))complete;
-(void)getListHotelWithDistrictId:(NSInteger )districtId complete:(void(^)(BOOL success, id responseObject))complete;
-(void)loadHotelDetailWithHotelId:(NSInteger )hotelId withDate:(NSString *)date complete:(void(^)(BOOL success, id responseObject))complete;
-(void)sendOrderTo123Pay:(NSDictionary *)params complete:(void(^)(BOOL success, id responseObject))complete;
-(void)wait123PayConfirm:(NSDictionary *)params complete:(void(^)(BOOL success, id responseObject))complete;
-(void)createPaymentWithParams:(NSDictionary *)params complete:(void(^)(BOOL success, id responseObject))complete;
-(void)checkOrderSuccessWithTransactionId:(NSString *)transactionId complete:(void(^)(BOOL success, id responseObject))complete;
// Old API
- (void)loginWithUsername:(NSString *)username password:(NSString *)password complete:(void(^)(BOOL success, id responseObject))complete;
- (void)registerUserWithUsername:(NSString *)username password:(NSString *)password complete:(void(^)(BOOL success, id responseObject))complete;

- (void)verifyUserWithId:(NSString *)userId code:(NSString *)code andResetPassword:(BOOL)resetPassword complete:(void(^)(BOOL success, id responseObject))complete;

- (void)resetPasswordWithPhoneNumber:(NSString *)phoneNumber complete:(void(^)(BOOL success, id responseObject))complete;

-(void)getListHotelWithDistrict:(NSInteger )districtId andPage:(NSInteger )page complete:(void(^)(BOOL success, id responseObject))complete;

- (void)confimBookingWithHotel:(NSInteger )hotelId andRoom:(NSInteger )roomId checkIn:(NSString *)checkIn checkOut:(NSString *)checkOut time:(NSInteger )time executedTime:(NSInteger )executedTime phoneUser:(NSString *)phone amount:(float )amount transactionId:(NSString *)transactionId complete:(void(^)(BOOL success, id responseObject))complete;

- (void)getListHistoryWithUserId:(NSInteger )userId complete:(void(^)(BOOL success, id responseObject))complete;
- (void)checkAvailableRoomsInHotel:(NSInteger )hotelId andRoom:(NSInteger )roomId checkIn:(NSString *)checkIn checkOut:(NSString *)checkOut time:(float )time executedTime:(NSInteger )executedTime complete:(void(^)(BOOL success, id responseObject))complete;
@end