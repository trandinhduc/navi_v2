//
//  Constants.h
//  Navi
//
//  Created by LAzyGuy on 3/17/16.
//  Copyright © 2016 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

// 123 Pay
#define BANK_CODE /*@"123PCC"*/@"123PAY"
#define CANCEL_URL @"http://navi.com.vn"
#define REDIRECT_URL @"http://navi.com.vn"
#define ERROR_URL @"http://navi.com.vn"

FOUNDATION_EXPORT NSString * urlCreateOrder123Pay;
FOUNDATION_EXPORT NSString * urlQueryOrder123Pay;
FOUNDATION_EXPORT NSString * merchantCode;
FOUNDATION_EXPORT NSString * passcode;
FOUNDATION_EXPORT NSString * secrectCode;
FOUNDATION_EXPORT NSString * clientIp;

#ifndef Language_Changer_Constants_h
#define Language_Changer_Constants_h

// NSUserDefaults keys
#define DEFAULTS_KEY_LANGUAGE_CODE @"LanguageCode" // The key against which to store the selected language code.

/*
 * Custom localised string macro, functioning in a similar way to the standard NSLocalisedString().
 */
#define CustomLocalisedString(key, comment) \
[[LanguageManager sharedLanguageManager] getTranslationForKey:key]

#endif