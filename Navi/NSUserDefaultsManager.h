//
//  NSUserDefaultsManager.h
//  SmartWave
//
//  Created by Si Nguyen on 4/13/15.
//  Copyright (c) 2015 Origin Agency. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hotel.h"
#define USER_ID @"userId"
#define USER_LOGIN @"isLogin"
#define LAST_HOTEL @"lastHotel"

@interface NSUserDefaultsManager : NSObject

+ (void)setUserId:(NSString *)userId;
+ (NSString *)getUserId;
+ (void)setUserLogin:(BOOL)isLogin;
+ (BOOL)hasUserData;
+ (BOOL)isLogin;

+(void)setLastHotel:(NSMutableDictionary *)dict;
+(Hotel *)getLastHotel;
@end
