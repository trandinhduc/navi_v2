//
//  NVCacheManager.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/14/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVCacheManager.h"

@implementation NVCacheManager


-(void)setLastHotel:(Hotel *)lastHotel{
    if (lastHotel == nil) {
        return;
    }
    NSMutableDictionary *dict = [[[NSUserDefaults standardUserDefaults]valueForKey:LAST_HOTEL] mutableCopy];
    if ([dict valueForKey:[NSUserDefaultsManager getUserId]]) {
        [dict removeObjectForKey:[NSUserDefaultsManager getUserId]];
    }
    if (dict == nil) {
        dict = [NSMutableDictionary new];
        [dict setValue:[Hotel convertHotelToDictionary:lastHotel] forKey:[NSUserDefaultsManager getUserId]];
    }else{
        [dict setValue:[Hotel convertHotelToDictionary:lastHotel] forKey:[NSUserDefaultsManager getUserId]];
    }
    [NSUserDefaultsManager setLastHotel:dict];
    _lastHotel = lastHotel;
}

-(NSMutableArray *)listDistrict{
    if (!_listDistrict) {
        _listDistrict = [NSMutableArray new];
    }
    return _listDistrict;
}

-(NSMutableArray *)listAccommodation{
    if (!_listAccommodations) {
        _listAccommodations = [NSMutableArray new];
    }
    return _listAccommodations;
}

-(NSString *)naviDesciption{
    if (!_naviDesciption) {
        _naviDesciption = @"";
    }
    return _naviDesciption;
}


#pragma mark - Create name for Screen


-(NSString *)HomeNibName{
    if (IS_IPHONE_6P) {
        _HomeNibName = @"NVHomeViewControllerIp6Plus";
        _ListHotelViewName = @"NVListHotelViewControllerIp6Plus";
        _DetailHotelName = @"NVDetailHotelViewControllerIp6Plus";
        _DetailHotelName_RoomOverView = @"NVRoomOverViewIp6Plus";
        _DetailHotelName_BottomView = @"NVBottomHotelDetailIp6Plus";
        _RoomDetailView = @"NVRoomDetailViewControllerIp6Plus";
        _RoomDetailView_Information = @"NVRoomDetailInformationIp6Plus";
        _RoomDetailView_Accomodation = @"NVCollectionAccommodationIp6Plus";
        _BookingViewName = @"NVEnterBookingViewControllerIp6Plus";
        _ConfirmInputPhoneName = @"NVConfirmPhoneViewIp6Plus";
        _SupportViewName = @"SupportViewControllerIp6Plus";
		_finishPaymentName = @"NVFinishPaymentViewIp6Plus";
        _AboutViewName = @"AboutViewControllerIp6Plus";
    }else if (IS_IPHONE_6){
        _HomeNibName = @"NVHomeViewControllerIp6";
        _ListHotelViewName = @"NVListHotelViewControllerIp6";
        _DetailHotelName = @"NVDetailHotelViewControllerIp6";
        _DetailHotelName_RoomOverView = @"NVRoomOverViewIp6";
        _DetailHotelName_BottomView = @"NVBottomHotelDetailIp6";
        _RoomDetailView = @"NVRoomDetailViewControllerIp6";
        _RoomDetailView_Information = @"NVRoomDetailInformationIp6";
        _RoomDetailView_Accomodation = @"NVCollectionAccommodationIp6";
        _BookingViewName = @"NVEnterBookingViewControllerIp6";
        _ConfirmInputPhoneName = @"NVConfirmPhoneViewIp6";
		_finishPaymentName = @"NVFinishPaymentViewIp6";
        _SupportViewName = @"SupportViewControllerIp6";
        _AboutViewName = @"AboutViewControllerIp6";
    }else{
        _HomeNibName = @"NVHomeViewController";
        _ListHotelViewName = @"NVListHotelViewController";
        _DetailHotelName = @"NVDetailHotelViewController";
        _DetailHotelName_RoomOverView = @"NVRoomOverView";
        _DetailHotelName_BottomView = @"NVBottomHotelDetail";
        _RoomDetailView = @"NVRoomDetailViewController";
        _RoomDetailView_Information = @"NVRoomDetailInformation";
        _RoomDetailView_Accomodation = @"NVCollectionAccommodation";
        _BookingViewName = @"NVEnterBookingViewController";
        _ConfirmInputPhoneName = @"NVConfirmPhoneView";
		_finishPaymentName = @"NVFinishPaymentViewIp6";
        _SupportViewName = @"SupportViewController";
        _AboutViewName = @"AboutViewController";
    }
    return _HomeNibName;
}
@end
