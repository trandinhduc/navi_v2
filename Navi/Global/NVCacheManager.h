//
//  NVCacheManager.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/14/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hotel.h"
#import "DOSingleton.h"

@interface NVCacheManager : DOSingleton

@property (nonatomic) Hotel *lastHotel;
@property (nonatomic) NSMutableArray *listAccommodations;
@property (nonatomic) NSMutableArray *listDistrict;
@property (nonatomic) NSString *accommodationImageUrl;
@property (nonatomic) NSString *hotelImageUrl;
@property (nonatomic) NSString *roomImageUrl;
@property (nonatomic) NSString *naviDesciption;
@property (nonatomic) NSString *supportEmail;

// Name of xib file

@property (nonatomic) NSString *HomeNibName;

@property (nonatomic) NSString *ListHotelViewName;
@property (nonatomic) NSString *DetailHotelName;
@property (nonatomic) NSString *DetailHotelName_RoomOverView;
@property (nonatomic) NSString *DetailHotelName_BottomView;
@property (nonatomic) NSString *RoomDetailView;

@property (nonatomic) NSString *RoomDetailView_Information;
@property (nonatomic) NSString *RoomDetailView_Accomodation;

@property (nonatomic) NSString *BookingViewName;

@property (nonatomic) NSString *ConfirmInputPhoneName;
@property (nonatomic) NSString *finishPaymentName;
@property (nonatomic) NSString *SupportViewName;
@property (nonatomic) NSString *AboutViewName;
@end

