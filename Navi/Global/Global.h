//
//  Global.h
//  waitrr
//
//  Created by An Nguyen on 9/8/15.
//  Copyright (c) 2015 Geekup. All rights reserved.
//
//#ifdef __OBJC__
#import <Foundation/Foundation.h>
#import "Utils.h"
#import "NVCacheManager.h"
#import "Utils.h"
#import <AFNetworking/AFNetworking.h>
#import "NetworkingManager.h"
#import "UIImageView+AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSUserDefaultsManager.h"
#import "NaviNavigationViewController.h"
#import <AddressBook/AddressBook.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationService.h"
#import "Constants.h"
#import "LanguageManager.h"
#import "Locale.h"
//#endif

#define CACHEPATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define __DEBUG_MODE__ false

#if __DEBUG_MODE__

#define ROOT_URL        @"http://navi.geekup.vn/api/v1/" //@"http://private-54f3e-test5202.apiary-mock.com/"
#define IMAGE_URL       @"http://navi.geekup.vn/uploads/images/"

#else

#define ROOT_URL        @"http://cms-test.navi.com.vn/api/v1/"
#define IMAGE_URL       @"http://cms-test.navi.com.vn/uploads/images/"

#endif

//API
#define MAP_API_KEY @"AIzaSyA_Crob7DlzSzS4AB5bQZ7Ihb-xIAMhvHc"

#define API_CONFIG                  @"user/appConfig"
#define API_LIST_HOTEL              @"hotel/list"
#define API_DETAIL_HOTEL            @"hotel/detail"
#define API_AVAILABLE_ROOM          @"hotel/checkRoomAvailable"
#define API_CREATE_PAYMENT          @"hotel/createPayment"
#define API_CHECK_PAYMENT           @"hotel/checkPaymentResult"
#define API_REGISTER                @"user/register"
#define API_VERIFY                  @"user/verify"
#define API_RESET_PASSWORD          @"user/resetPassword"
#define API_CONFIM_BOOKING          @"hotel/booking"
#define API_LIST_HISTORY            @"hotel/bookingHistory"









