//
//  LocationService.h
//  FiBu
//
//  Created by thinhvoxuan on 7/3/15.
//  Copyright (c) 2015 Tran Dinh Duc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^GetGeocoderComplete)(CLPlacemark *geoInfo);


@interface LocationService : NSObject<CLLocationManagerDelegate>
- (CLLocationCoordinate2D) currentLocation;
- (void) startLocationService;
- (double) getLaTitute;
- (double) getLongTitude;

@property (readwrite, nonatomic, copy) GetGeocoderComplete getGeocoderComplete;
@property (nonatomic) CLAuthorizationStatus currentStatus;
-(BOOL)checkLocation;
@property (nonatomic) BOOL isCheckStatus;
@end
