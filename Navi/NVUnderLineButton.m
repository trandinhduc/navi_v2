//
//  WTUnderLineButton.m
//  waitrr
//
//  Created by An Nguyen on 9/3/15.
//  Copyright (c) 2015 Geekup. All rights reserved.
//

#import "NVUnderLineButton.h"

@implementation NVUnderLineButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib{
    [super awakeFromNib];
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setAlignment:NSTextAlignmentCenter];
    [style setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSDictionary *dict = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle),
                            NSForegroundColorAttributeName:self.titleLabel.textColor,
                            NSFontAttributeName:self.titleLabel.font,
                            NSParagraphStyleAttributeName:style}; // Added line
    
    [self setAttributedTitle: [[NSAttributedString alloc] initWithString:self.titleLabel.text attributes:dict] forState:UIControlStateNormal];;
}

@end
