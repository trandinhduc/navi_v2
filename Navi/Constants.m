//
//  Constants.m
//  Navi
//
//  Created by LAzyGuy on 3/17/16.
//  Copyright © 2016 Duc Tran. All rights reserved.
//

#import "Constants.h"

NSString * urlCreateOrder123Pay = @"https://sandbox.123pay.vn/miservice/createOrder1";
NSString * urlQueryOrder123Pay = @"https://sandbox.123pay.vn/miservice/queryOrder1";
NSString * merchantCode = @"MICODE";
NSString * passcode = @"MIPASSCODE";
NSString * secrectCode=@"MIKEY";
NSString * clientIp = @"127.0.0.1";