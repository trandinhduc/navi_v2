//
//  NetworkingManager.m
//  FiBu
//
//  Created by An Nguyen on 7/8/15.
//  Copyright (c) 2015 Tran Dinh Duc. All rights reserved.
//

#import "NetworkingManager.h"

@interface NetworkingManager()
@property (nonatomic, strong) AFHTTPRequestOperationManager *httpRequestOperationManager;
@end

@implementation NetworkingManager

#pragma mark - private methods

- (AFHTTPRequestOperationManager *)httpRequestOperationManager{
    if (!_httpRequestOperationManager) {
        _httpRequestOperationManager = [[AFHTTPRequestOperationManager alloc] init];
        _httpRequestOperationManager.responseSerializer.acceptableContentTypes = [_httpRequestOperationManager.responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"application/zip",@"application/json", @"text/html"]];
        _httpRequestOperationManager.operationQueue.maxConcurrentOperationCount = 10;
        [_httpRequestOperationManager.requestSerializer setTimeoutInterval:180.0];
        [_httpRequestOperationManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    }
    return _httpRequestOperationManager;
}

- (NSString *) genUrlFromString:(NSString *) path{
    return [NSString stringWithFormat:@"%@%@", ROOT_URL, path];
}

#pragma mark - New API

- (void)loadConfigFromServer:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_CONFIG];
    [self.httpRequestOperationManager GET:url
                               parameters:nil
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
                                          complete(YES,responseObject[@"response"]);
                                      }else{
                                          complete(NO,responseObject[@"response"]);
                                      }
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      complete(NO, error);
                                  }];

}

- (void)getListHotelWithDistrictId:(NSInteger )districtId complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_LIST_HOTEL];
    NSDictionary *params = @{
                             @"district":@(districtId)
                             };
    [self.httpRequestOperationManager GET:url
                               parameters:params
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
                                          complete(YES,responseObject[@"response"]);
                                      }else{
                                          complete(NO,responseObject[@"response"]);
                                      }
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      complete(NO, error);
                                  }];
}

- (void)loadHotelDetailWithHotelId:(NSInteger )hotelId withDate:(NSString *)date complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_DETAIL_HOTEL];
    NSDictionary *params = @{
                             @"id":@(hotelId),
                             @"date":date
                             };
    [self.httpRequestOperationManager GET:url
                               parameters:params
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
                                          complete(YES,responseObject[@"response"]);
                                      }else{
                                          complete(NO,responseObject[@"response"]);
                                      }
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      complete(NO, error);
                                  }];

}

- (void)createPaymentWithParams:(NSDictionary *)params complete:(void (^)(BOOL, id))complete{
	NSString *url = [self genUrlFromString:API_CREATE_PAYMENT];
	NSLog(@"Create payment: %@\n%@",url,params);
	[self.httpRequestOperationManager POST:url
							   parameters:params
								  success:^(AFHTTPRequestOperation *operation, id responseObject) {
									  if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
										  complete(YES,responseObject[@"response"]);
									  }else{
										  complete(NO,responseObject[@"response"]);
									  }
								  }
								  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
									  complete(NO, error);
								  }];
	//    {
}

-(void)checkOrderSuccessWithTransactionId:(NSString *)transactionId complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_CHECK_PAYMENT];
    NSDictionary *params = @{
                             @"transaction_id":transactionId
                             };
    [self.httpRequestOperationManager GET:url
                               parameters:params
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
                                          complete(YES,responseObject[@"response"]);
                                      }else{
                                          complete(NO,responseObject[@"response"]);
                                      }
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      complete(NO, error);
                                  }];
//    {
//        meta =     {
//            code = 200;
//            message = "Your request has been completed";
//            success = 1;
//        };
//        response =     {
//            error = "PAYMENT_NOT_FOUND";
//            success = 0;
//        };
//    }
    
}


-(void)sendOrderTo123Pay:(NSDictionary *)params complete:(void(^)(BOOL success, id responseObject))complete{
	NSString *url = urlCreateOrder123Pay;
	
	AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
	[serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	NSURLRequest *request =  [serializer requestWithMethod:@"POST" URLString:url parameters:params error:nil];
	
	AFHTTPRequestOperation *createOrderOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	[createOrderOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
		
		NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
		NSInteger returnCode = [arr[0]integerValue];
		if (returnCode == 1) {
			complete(YES,arr);
		}else{
			complete(NO,arr[1]);
		}
	} failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
		complete(NO, error);
	}];
	
	[createOrderOperation start];
}

- (void)wait123PayConfirm:(NSDictionary *)params complete:(void(^)(BOOL success, id responseObject))complete{
	NSString *url = urlQueryOrder123Pay;
	
	AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
	[serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
	
	NSURLRequest *request =  [serializer requestWithMethod:@"POST" URLString:url parameters:params error:nil];
	
	AFHTTPRequestOperation *createOrderOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	[createOrderOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
		NSArray *arr = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
		NSInteger returnCode = [arr[0]integerValue];
	   if (returnCode == 1) {
		   complete(YES,arr);
	   }else{
		   complete(NO,arr[1]);
	   }
	} failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
		complete(NO, error);
	}];
	
	[createOrderOperation start];
}

// --------------------------------------------------------------------------------
#pragma mark - Old API

- (void)loginWithUsername:(NSString *)username password:(NSString *)password complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_CONFIG];
    NSDictionary *params = @{
                             @"phone":username,
                             @"password":password
                             };
    [self.httpRequestOperationManager POST:url
                                parameters:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       if (responseObject[@"response"][@"success"] && [responseObject[@"response"][@"success"] boolValue]) {
                                           complete(YES,responseObject[@"response"]);
                                       }else{
                                           complete(NO,responseObject[@"response"]);
                                       }
                                   }
                                   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       complete(NO, error);
                                   }];
}

- (void)registerUserWithUsername:(NSString *)username password:(NSString *)password complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_REGISTER];
    NSDictionary *params = @{
                             @"phone":username,
                             @"password":password
                             };
    [self.httpRequestOperationManager POST:url
                                parameters:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       if (responseObject[@"response"][@"success"] && [responseObject[@"response"][@"success"] boolValue]) {
                                           complete(YES,responseObject[@"response"]);
                                       }else{
                                           complete(NO,responseObject[@"response"]);
                                       }
                                   }
                                   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       complete(NO, error);
                                   }];

}

- (void)verifyUserWithId:(NSString *)userId code:(NSString *)code andResetPassword:(BOOL)resetPassword complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_VERIFY];
    NSDictionary *params = @{
                             @"id":userId,
                             @"code":code,
                             @"resetPassword":@(resetPassword)
                             };
//    if (resetPassword) {
//        [params setValue:@"true" forKey:@"resetPassword"];
//    }
    [self.httpRequestOperationManager POST:url
                                parameters:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       if (responseObject[@"response"][@"success"] && [responseObject[@"response"][@"success"] boolValue]) {
                                           complete(YES,responseObject[@"response"]);
                                       }else{
                                           complete(NO,responseObject[@"response"]);
                                       }
                                   }
                                   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       complete(NO, error);
                                   }];

}

- (void)resetPasswordWithPhoneNumber:(NSString *)phoneNumber complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_RESET_PASSWORD];
    NSDictionary *params = @{
                             @"phone":phoneNumber
                             };
    [self.httpRequestOperationManager POST:url
                                parameters:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       if (responseObject[@"response"][@"success"] && [responseObject[@"response"][@"success"] boolValue]) {
                                           complete(YES,responseObject[@"response"]);
                                       }else{
                                           complete(NO,responseObject[@"response"]);
                                       }
                                   }
                                   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       complete(NO, error);
                                   }];

}

- (void)getListHotelWithDistrict:(NSInteger )districtId andPage:(NSInteger )page complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_LIST_HOTEL];
    NSDictionary *params = @{
                             @"district":@(districtId),
                             @"page":@(page)
                             };

    [self.httpRequestOperationManager GET:url
                               parameters:params
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                            if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
                                                complete(YES,responseObject[@"response"]);
                                            }else{
                                                complete(NO,responseObject[@"response"]);
                                            }
                                        }
                                        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                            complete(NO, error);
                                        }];
    
}

- (void)confimBookingWithHotel:(NSInteger )hotelId andRoom:(NSInteger)roomId checkIn:(NSString *)checkIn checkOut:(NSString *)checkOut time:(NSInteger)time executedTime:(NSInteger)executedTime phoneUser:(NSString *)phone amount:(float )amount transactionId:(NSString *)transactionId complete:(void (^)(BOOL, id))complete{
    NSString *url = [self genUrlFromString:API_CONFIM_BOOKING];
    NSDictionary *params = @{
                             @"user_id":@([[NSUserDefaultsManager getUserId]integerValue]),
                             @"hotel_id":@(hotelId),
                             @"room_id":@(roomId),
                             @"checkin":checkIn,
                             @"checkout":checkOut,
                             @"time":@(time),
							 @"executed_time":@(executedTime),
							 @"phone_user":phone,
							 @"amount":@(amount),
							 @"transaction_id":transactionId
                             };
    
    [self.httpRequestOperationManager POST:url
                                parameters:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       if (responseObject[@"response"][@"success"] && [responseObject[@"response"][@"success"] boolValue]) {
                                           complete(YES,responseObject[@"response"]);
                                       }else{
                                           complete(NO,responseObject[@"response"]);
                                       }
                                   }
                                   failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       complete(NO, error);
                                   }];
}

- (void)getListHistoryWithUserId:(NSInteger )userId complete:(void(^)(BOOL success, id responseObject))complete{
    NSString *url = [self genUrlFromString:API_LIST_HISTORY];
    NSDictionary *params = @{
                             @"user_id":@(userId)
                             };
    [self.httpRequestOperationManager GET:url
                               parameters:params
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
                                          complete(YES,responseObject[@"response"]);
                                      }else{
                                          complete(NO,responseObject[@"response"]);
                                      }
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      complete(NO, error);
                                  }];
}

- (void)checkAvailableRoomsInHotel:(NSInteger)hotelId
                           andRoom:(NSInteger)roomId
                           checkIn:(NSString *)checkIn
                          checkOut:(NSString *)checkOut
                              time:(float )time
                      executedTime:(NSInteger)executedTime
                          complete:(void (^)(BOOL, id))complete{
    NSString *url = [self genUrlFromString:API_AVAILABLE_ROOM];
    NSDictionary *params = @{
                             @"hotel_id":@(hotelId),
                             @"room_id":@(roomId),
                             @"checkin":checkIn,
                             @"checkout":checkOut,
                             @"time":@(time),
                             @"executed_time":@(executedTime)
                             };
    [self.httpRequestOperationManager POST:url
                               parameters:params
                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                      if (responseObject[@"meta"][@"success"] && [responseObject[@"meta"][@"success"] boolValue]) {
										  if ([responseObject[@"response"][@"roomAvailable"] boolValue]) {
											complete(YES,responseObject[@"response"]);
										  }else{
											  complete(NO,responseObject[@"response"]);
										  }
                                      }else{
                                          complete(NO,responseObject[@"response"]);
                                      }
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                      complete(NO, error);
                                  }];
}

@end