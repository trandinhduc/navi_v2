//
//  NVRoomOverView.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomDetail.h"

@protocol RoomDetailDelegate <NSObject>

-(void)clickRoomDetailWithRoomId:(RoomDetail *)roomDetail;

@end

@interface NVRoomOverView : UIView
@property (nonatomic, weak) id<RoomDetailDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *roomName;
@property (strong, nonatomic) IBOutlet UILabel *firstPrice;
@property (strong, nonatomic) IBOutlet UILabel *secondPrice;
@property (strong, nonatomic) IBOutlet UIImageView *roomImage;
@property (nonatomic) RoomDetail *roomDetail;
-(void)setupWithRoomDetail:(RoomDetail *)roomDeail;



@end
