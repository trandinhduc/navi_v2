//
//  NVRoomOverView.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVRoomOverView.h"
#import "RoomDetail.h"
@implementation NVRoomOverView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tapToBackground = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapBackground)];
    tapToBackground.numberOfTapsRequired = 1;
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:tapToBackground];
}

-(void)tapBackground{
    NSLog(@"Tapped");
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickRoomDetailWithRoomId:)]) {
        [self.delegate clickRoomDetailWithRoomId:_roomDetail];
    }
}

-(void)setupWithRoomDetail:(RoomDetail *)roomDeail{
    self.roomName.text = [roomDeail.name uppercaseString];
    if (![[_roomDetail.listPrice valueForKey:[self getHourString]] isMemberOfClass:[NSNull class]]) {
        NSDictionary *roomPriceDict = [NSDictionary new];
        NSString *day = [self getToDay];
//        NSString *day= @"Tue-Wed-Thu";
        if ([@"Tue-Wed-Thu" rangeOfString:day].location == NSNotFound) {
            
            roomPriceDict = [_roomDetail.listPrice valueForKey:day];
        }else{
            roomPriceDict = [_roomDetail.listPrice valueForKey:@"Tue-Wed-Thu"];

        }
        
        NSString *price = [roomPriceDict valueForKey:[self getHourString]];
        self.firstPrice.text = [NSString stringWithFormat:@"%@ Đ/Giờ",[Utils convertPriceIntegerToString:price]];
    }else{
        self.firstPrice.text = @"";
    }
    
//    self.firstPrice.text = [NSString stringWithFormat:@"%@ Đ/Giờ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",[[[roomDeail.prices sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
//		return [obj1 integerValue] < [obj2 integerValue];
//	}] firstObject] integerValue]]]];
//    self.secondPrice.text = [NSString stringWithFormat:@"%@ Đ/Giờ",[Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",[[[roomDeail.prices sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
//		return [obj1 integerValue] < [obj2 integerValue];
//	}] objectAtIndex:1] integerValue]]]];
	NSLog(@"Room \"%@\" with iamge: %@",self.roomName.text, [Utils imageRoomUrl:roomDeail.images[0]]);
	[Utils imageView:self.roomImage setImageWithURL:[Utils imageRoomUrl:roomDeail.images[0]] placeholder:[UIImage imageNamed:@"hotel_default"]];
}

-(NSString *)getToDay{
    NSString *toDay;
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setDateFormat:@"EEE"];
    [formatter setLocale:usLocale];
    toDay = [formatter stringFromDate:date];
    return toDay;
}

-(NSString *)getHourString{
    NSString *hourString;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH"];
    hourString = [formatter stringFromDate:[NSDate date]];
    if ([hourString integerValue] <10) {
        hourString = [hourString stringByReplacingOccurrencesOfString:@"0" withString:@""];
    }
    return hourString;
    
}
@end
