//
//  NVTextFieldClean.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/9/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NVTextFieldClean : UITextField
@property (nonatomic) UIImage *leftImage;
@end
