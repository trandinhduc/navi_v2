//
//  NVConfirmCode.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/12/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NVConfirmBoxDelegate <NSObject>

-(void)confirmSuccess;

@end

@interface NVConfirmCode : UIView

@property (nonatomic, weak) id<NVConfirmBoxDelegate> delegate;

@property (strong, nonatomic) IBOutlet UITextField *passcodeTextfield;
@property (nonatomic) BOOL resetPassword;
- (IBAction)clickConfirmButton:(id)sender;
@end
