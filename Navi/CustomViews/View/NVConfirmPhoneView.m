//
//  NVConfirmPhoneView.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/9/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVConfirmPhoneView.h"
#define MAXLENGTH 11
@implementation NVConfirmPhoneView

-(void)awakeFromNib{
    [super awakeFromNib];
    _confirmView.layer.cornerRadius = 5;
    _confirmView.clipsToBounds = YES;
    
    [_textfieldPhone becomeFirstResponder];
    [_textfieldPhone.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [_textfieldPhone.layer setBorderWidth:1.5];
    _textfieldPhone.delegate = self;
    //The rounded corner part, where you specify your view's corner radius:
    _textfieldPhone.layer.cornerRadius = 2;
    _textfieldPhone.clipsToBounds = YES;
}

- (IBAction)clickFinishButton:(UIButton *)sender {
    if (_textfieldPhone.text.length < 10) {
        UIAlertView *alertMinimun = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Số điện thoại không đúng. Hãy nhập lại!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertMinimun show];
        return;
    }
    [self endEditing:YES];
    if(self.textfieldPhone.text.length >0){
        if (self.delegate && [self.delegate respondsToSelector:@selector(bookingWithPhoneNumber:)]) {
            [self.delegate bookingWithPhoneNumber:self.textfieldPhone.text];
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Số điện thoại không được phép để trống" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
- (IBAction)clickOutSide:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickOutSideConfirmPhone)]) {
        [self.delegate clickOutSideConfirmPhone];
    }
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLENGTH || returnKey;
}

@end
