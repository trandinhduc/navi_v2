//
//  NVCollectionAccommodation.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/3/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVCollectionAccommodation.h"
#import "NVAccommodationCell.h"
#import "Accomodation.h"

@implementation NVCollectionAccommodation 

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib{
    [super awakeFromNib];
    [self initCollectionView];
}

-(void)initCollectionView{
    _itemHei = _collectionAccommodation.frame.size.height/3 - 10;
    [self.collectionAccommodation registerNib:[UINib nibWithNibName:@"NVAccommodationCell" bundle:nil] forCellWithReuseIdentifier:@"CollectionAccommodation"];
    self.collectionAccommodation.dataSource = self;
    self.collectionAccommodation.delegate = self;
    
    [self.collectionAccommodation reloadData];
}

- (void)setItems:(NSArray *)items{
    [self.collectionAccommodation addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    _items = items;
    
    [self.collectionAccommodation reloadData];
}

- (void)dealloc{
    [self.collectionAccommodation removeObserver:self forKeyPath:@"contentSize" context:nil];
}

#pragma mark - Collectionview delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _items.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NVAccommodationCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionAccommodation" forIndexPath:indexPath];
    Accomodation *acco = self.items[indexPath.item];
	
	[Utils imageView:cell.iconImageView setImageWithURL:[Utils iconAccommodationUrl:acco.icon] placeholder:nil];

    cell.nameLabel.text = acco.name;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width = collectionView.frame.size.width;
    return CGSizeMake(width/3 - 10,_itemHei);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(0, 0, 0, 0);
//}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"contentSize"]) {
        float contentSizeHei = self.collectionAccommodation.contentSize.height;
        if (contentSizeHei < _itemHei) {
            contentSizeHei = _itemHei;
        }
        CGRect frame = self.collectionAccommodation.frame;
        frame.size.height = contentSizeHei;
        self.collectionAccommodation.frame = frame;

        frame = self.frame;
        frame.size.height = CGRectGetMaxY(self.collectionAccommodation.frame);
        self.frame = frame;
        if (self.delegate && [self.delegate respondsToSelector:@selector(accomodationViewUpdateedHeight:)]) {
            [self.delegate accomodationViewUpdateedHeight:contentSizeHei];
        }
    }
}

@end
