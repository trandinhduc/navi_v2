//
//  NVBottomHotelDetail.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "HotelDetail.h"
@interface NVBottomHotelDetail : UIView <MKMapViewDelegate>
@property (nonatomic) HotelDetail *hotelDetail;
@property (strong, nonatomic) IBOutlet UITextView *hotelDescription;
@property (strong, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) IBOutlet MKMapView *appleMapView;

-(void)initMapsAndDescription;
@end
