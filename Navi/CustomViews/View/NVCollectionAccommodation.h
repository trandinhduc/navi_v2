//
//  NVCollectionAccommodation.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/3/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomDetail.h"

@protocol NVCollectionAccommodationDelegate <NSObject>
@optional
- (void)accomodationViewUpdateedHeight:(float)hei;

@end

@interface NVCollectionAccommodation : UIView <UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic) RoomDetail *roomDetail;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionAccommodation;

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) id<NVCollectionAccommodationDelegate> delegate;
@property (nonatomic) float itemHei;
@end
