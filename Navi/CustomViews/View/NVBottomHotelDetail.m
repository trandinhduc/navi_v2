//
//  NVBottomHotelDetail.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/2/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVBottomHotelDetail.h"

@implementation NVBottomHotelDetail 

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(HotelDetail *)hotelDetail{
    if (!_hotelDetail) {
        _hotelDetail = [HotelDetail new];
    }
    return _hotelDetail;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    _appleMapView.delegate = self;
    UITapGestureRecognizer *clickMap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(openMap)];
    [self.mapView addGestureRecognizer:clickMap];
 

}

-(void)openMap{
    NSString *nativeMapScheme = @"maps.apple.com";
    NSString* url = [NSString stringWithFormat:@"http://%@/maps?q=%@,%@", nativeMapScheme, _hotelDetail.latitude ,_hotelDetail.longitude];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(void)initMapsAndDescription{
    [_hotelDescription setText:_hotelDetail.hotelDescription];
    //    MKMapView *mapView = [[MKMapView alloc] initWithFrame:_mapViewLocation.bounds];
    
    //    _mapViewLocation.backgroundColor = [UIColor redColor];
    
    _appleMapView.delegate = self;
    _appleMapView.mapType = MKMapTypeStandard;
    
    MKCoordinateRegion viewRegion = {{0.0,0.0},{0.0,0.0}};
    viewRegion.center.latitude = [_hotelDetail.latitude floatValue];
    viewRegion.center.longitude = [_hotelDetail.longitude floatValue];
    viewRegion.span.latitudeDelta = 0.005f;
    viewRegion.span.longitudeDelta = 0.005f;
    if (CLLocationCoordinate2DIsValid(viewRegion.center)) {
        [_appleMapView setRegion:viewRegion animated:YES];
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = viewRegion.center;
        point.title = _hotelDetail.hotelName;
        //    point.subtitle = _hotelDetail.address;
        [_appleMapView addAnnotation:point];
    }
}

@end
