//
//  NVConfirmCode.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/12/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVConfirmCode.h"

@implementation NVConfirmCode

- (IBAction)clickConfirmButton:(id)sender {
    NSString *passcode = [self.passcodeTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (passcode.length == 0) {
        UIAlertView *passcodeAlert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Vui lòng nhập đầy đủ thông tin!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [passcodeAlert show];
        return;
    }
    [[Utils getInstance]shouldShowProgressHUD:YES WithText:@""];
    [[NetworkingManager sharedInstance]verifyUserWithId:[NSUserDefaultsManager getUserId] code:passcode andResetPassword:self.resetPassword complete:^(BOOL success, id responseObject) {
        [[Utils getInstance]shouldShowProgressHUD:NO WithText:@""];
        if (success) {
            // need handle
            if (self.delegate && [self.delegate respondsToSelector:@selector(confirmSuccess)]) {
                [self.delegate confirmSuccess];
            }
//             [self removeFromSuperview];
        }else{
            UIAlertView *passcodeAlert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Xác nhận tài khoản không thành công. Vui lòng thử lại." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [passcodeAlert show];
            return;
        }
    }];
}
@end
