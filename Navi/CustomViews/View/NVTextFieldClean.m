//
//  NVTextFieldClean.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/9/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVTextFieldClean.h"

@implementation NVTextFieldClean{
    CALayer *border;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib{
    [super awakeFromNib];
//    self.layer.cornerRadius = 8.f;
    
    border = [CALayer layer];
    
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, self.frame.size.height);
    border.borderWidth = borderWidth;
    [self.layer addSublayer:border];
    self.layer.masksToBounds = YES;
    
    [self setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];

    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
    
    NSLog(@"1");
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = [[UIImageView alloc] initWithImage:_leftImage];
//    self.leftView.frame = CGRectMake(0, 0, 45, 45);
    self.leftView.backgroundColor = [UIColor redColor];
}

-(void)setLeftImage:(UIImage *)leftImage{
     NSLog(@"2");
    if (leftImage) {
        self.leftView = [[UIImageView alloc] initWithImage:leftImage];
    }
}

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, CGRectGetMaxX(self.leftView.frame) + 8, 0);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, CGRectGetMaxX(self.leftView.frame) + 8, 0);
}

-(void)layoutSubviews{
    [super layoutSubviews];
    border.frame = CGRectMake(0, self.frame.size.height - 1, self.frame.size.width, self.frame.size.height);
//    self.leftView = [[UIImageView alloc] initWithImage:_leftImage];

}


@end
