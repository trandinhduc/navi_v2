//
//  NVRoomDetailInformation.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/3/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVRoomDetailInformation.h"

@interface NVRoomDetailInformation()
@property (nonatomic, weak) IBOutlet UILabel *areaLabel;
@property (nonatomic, weak) IBOutlet UILabel *bedTypeLavel;
@property (nonatomic, weak) IBOutlet UILabel *bathTubLabel;
@property (nonatomic, weak) IBOutlet UILabel *verticalBathRoomLabel;
@end

@implementation NVRoomDetailInformation

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)updateInfo:(RoomDetail *)roomDetail{
	self.bedTypeLavel.text = roomDetail.bedType;
	UIFont *fontEverySmall = [UIFont fontWithName:@"SourceSansPro-Bold" size:10];

	NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:roomDetail.area
																						 attributes:@{NSFontAttributeName: self.areaLabel.font}];
	[attributedString setAttributes:@{ NSFontAttributeName : fontEverySmall,
									   NSBaselineOffsetAttributeName : @6
									   }
							  range:NSMakeRange(roomDetail.area.length - 1, 1)];
	self.areaLabel.attributedText = attributedString;

	if (roomDetail.bathtub == 0) {
		self.bathTubLabel.text = @"Không";
	}else{
		self.bathTubLabel.text = @"Có";
	}
	
	if (roomDetail.standingBathtub == 0) {
		self.verticalBathRoomLabel.text = @"Không";
	}else{
		self.verticalBathRoomLabel.text = @"Có";
	}
}

@end
