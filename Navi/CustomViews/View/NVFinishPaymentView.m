//
//  NVFinishPaymentView.m
//  Navi
//
//  Created by Tran Dinh Duc on 12/11/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVFinishPaymentView.h"

@implementation NVFinishPaymentView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib{
    [super awakeFromNib];
    _finishView.layer.cornerRadius = 5;
    _finishView.clipsToBounds = YES;
}


- (IBAction)clickFinishButton:(UIButton *)sender {
    [self removeFromSuperview];
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickFinish)]) {
        [self.delegate clickFinish];
    }
}
@end
