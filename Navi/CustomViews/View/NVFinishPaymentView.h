//
//  NVFinishPaymentView.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/11/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NVFinishPaymentDelegate <NSObject>

-(void)clickFinish;
@end

@interface NVFinishPaymentView : UIView
@property (nonatomic, weak) id<NVFinishPaymentDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *finishView;
- (IBAction)clickFinishButton:(UIButton *)sender;

@end
