//
//  NVConfirmPhoneView.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/9/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol NVConfirmPhoneDelegate <NSObject>

-(void)bookingWithPhoneNumber:(NSString *)phoneNumber;
-(void)clickOutSideConfirmPhone;
@end

@interface NVConfirmPhoneView : UIView <UITextFieldDelegate>
- (IBAction)clickOutSide:(UIButton *)sender;

@property (nonatomic, weak) id<NVConfirmPhoneDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *confirmView;
- (IBAction)clickFinishButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextField *textfieldPhone;

@end
