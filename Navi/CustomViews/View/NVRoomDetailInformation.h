//
//  NVRoomDetailInformation.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/3/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomDetail.h"

@interface NVRoomDetailInformation : UIView

- (void)updateInfo:(RoomDetail *)roomDetail;

@end
