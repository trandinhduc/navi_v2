//
//  NVRoomImageCell.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/3/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol RoomImageDelegate <NSObject>
//
//-(void)clickRoomImageWithIndex:(NSInteger )imageIndex;
//
//@end

@interface NVRoomImageCell : UICollectionViewCell
//@property (nonatomic, weak) id<RoomImageDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIImageView *image;

@end
