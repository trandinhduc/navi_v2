//
//  NVHotelViewCell.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVHotelViewCell.h"

@implementation NVHotelViewCell

- (void)awakeFromNib {
    // Initialization code
    self.imageHotel.image = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setupCellWithHotelItem:(Hotel *)hotelItems{
    self.nameLabel.text = [hotelItems.name uppercaseString];
    self.addressLabel.text = hotelItems.address;
    self.priceLabel.text = [NSString stringWithFormat:@"%@ Đ",hotelItems.price];
	[Utils imageView:self.imageHotel setImageWithURL:[Utils thumbnailHotelUrl:hotelItems.image] placeholder:[UIImage imageNamed:@"hotel_default"]];

//	__weak UIImageView *weakImageView = self.imageHotel;
//	weakImageView.image = [UIImage imageNamed:@"hotel_default"];
//	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//		[self.imageHotel setImageWithURLRequest:[NSURLRequest requestWithURL:[Utils thumbnailHotelUrl:hotelItems.image] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30.f] placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
//			dispatch_async(dispatch_get_main_queue(), ^{
//				weakImageView.image = image;
//			});
//		} failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
//			
//		}];
//	});
    NSString *starString = @"";
    _star = hotelItems.star;
    switch (_star) {
        case 1:
            starString = [NSString stringWithUTF8String:"\u2605 "];
            break;
        case 2:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 "];
            break;
        case 3:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 "];
            break;
        case 4:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 \u2605 "];
            break;
        case 5:
            starString = [NSString stringWithUTF8String:"\u2605 \u2605 \u2605 \u2605 \u2605 "];
            break;
        default:
            break;
    }
    self.starLabel.text = starString;
}



@end
