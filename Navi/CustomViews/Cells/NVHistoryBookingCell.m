//
//  NVHistoryBookingCell.m
//  Navi
//
//  Created by Tran Dinh Duc on 10/15/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import "NVHistoryBookingCell.h"

@implementation NVHistoryBookingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setupHistoryCell:(HistoryBooking *)historyItem{
   NSArray  *arrayListDistrict = @[@"Quận 1",@"Quận 2",@"Quận 3",@"Quận 4",@"Quận 5",@"Quận 6",@"Quận 7",@"Quận 8",@"Quận 9",@"Quận 10",@"Quận 11",@"Quận 12",@"Quận Bình Tân",@"Quận Bình Thạnh",@"Quận Gò Vấp",@"Quận Phú Nhuận",@"Quận Tân Bình",@"Quận Tân Phú",@"Quận Thủ Đức",@"Quận Bình Chánh",@"Quận Cần Giờ",@"Quận Củ Chi",@"Quận Hóc Môn",@"Quận Nhà Bè"];
    
    _hotelnameLabel.text = historyItem.name;
    _districtLabel.text = arrayListDistrict[historyItem.district - 1];
    _checkInLabel.text = historyItem.checkIn;
    _timeLabel.text = [NSString stringWithFormat:@"%ld giờ",(long)historyItem.time];
    NSString *price = [Utils convertPriceIntegerToString:[NSString stringWithFormat:@"%ld",(long)historyItem.amount]];
    _amountLabel.text = [NSString stringWithFormat:@"%@ VNĐ",price];
    
    if (historyItem.status == 1) {
        _statusLabel.text = @"Chưa thanh toán";
    }else{
        _statusLabel.text = @"Đã thanh toán";
    }
}

@end
