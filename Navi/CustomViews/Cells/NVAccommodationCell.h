//
//  NVAccommodationCell.h
//  Navi
//
//  Created by Tran Dinh Duc on 12/3/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NVAccommodationCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@end
