//
//  NVHotelViewCell.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/13/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Hotel.h"
@interface NVHotelViewCell : UITableViewCell
//@property (strong, nonatomic) IBOutlet UIButton *bookNowButton;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
//@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageHotel;
@property (strong, nonatomic) IBOutlet UILabel *starLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (nonatomic ) NSInteger star;
-(void)setupCellWithHotelItem:(Hotel *)hotelItems;

@end
