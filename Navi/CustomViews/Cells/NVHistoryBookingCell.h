//
//  NVHistoryBookingCell.h
//  Navi
//
//  Created by Tran Dinh Duc on 10/15/15.
//  Copyright © 2015 Duc Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryBooking.h"
@interface NVHistoryBookingCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *hotelnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *districtLabel;
@property (strong, nonatomic) IBOutlet UILabel *checkInLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;

-(void)setupHistoryCell:(HistoryBooking *)historyItem;

@end
