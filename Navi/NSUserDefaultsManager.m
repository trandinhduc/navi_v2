//
//  NSUserDefaultsManager.m
//  SmartWave
//
//  Created by Si Nguyen on 4/13/15.
//  Copyright (c) 2015 Origin Agency. All rights reserved.
//

#import "NSUserDefaultsManager.h"

@implementation NSUserDefaultsManager

#pragma mark - Fisrt Launch

+ (void)setUserLogin:(BOOL)isLogin{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:isLogin forKey:USER_LOGIN];
    [defaults synchronize];
}

+ (void)setUserId:(NSString *)userId{
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:userId forKey:USER_ID];
    [defaults synchronize];
}

+ (NSString *)getUserId{
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults valueForKey:USER_ID]){
        return [defaults valueForKey:USER_ID];
    }else{
        return @"";
    }
}

+ (BOOL)hasUserData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return ([defaults objectForKey:USER_ID] ? YES : NO);
}

+ (BOOL)isLogin{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:USER_LOGIN]) {
        return [[defaults valueForKey:USER_LOGIN] length]>0?YES:NO;
    }
    return NO;
}

+ (void)setLastHotel:(NSMutableDictionary *)dict{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (dict) {
        [defaults setValue:dict forKey:LAST_HOTEL]; 
    }
}

// dict = @{
//         @"id" : @"dict"
//         @"id" : @"dict"
//         }

+(Hotel *)getLastHotel{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    Hotel *hotelItem;
    if ([defaults valueForKey:LAST_HOTEL]) {
        NSMutableDictionary *dict = [defaults valueForKey:LAST_HOTEL];
        if ([dict valueForKey:[NSUserDefaultsManager getUserId]]) {
            hotelItem = [Hotel convertJsonToHotel:[dict valueForKey:[NSUserDefaultsManager getUserId]]];
        }
        return hotelItem;
    }
    return hotelItem;
}
@end

